LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

iwds-res := ../iwds-ui-res-jar

src_dirs := src $(iwds-res)/src
res_dirs := res $(iwds-res)/res

LOCAL_RESOURCE_DIR := $(addprefix $(LOCAL_PATH)/, $(res_dirs))

LOCAL_AAPT_FLAGS := \
   --auto-add-overlay \
   --extra-packages com.ingenic.iwds.ui

LOCAL_SRC_FILES := $(call all-java-files-under, $(src_dirs)) $(call all-java-files-under, src)

LOCAL_STATIC_JAVA_LIBRARIES := iwds-service-ui-jar iwds-ui-with-res-jar zxing-jar

LOCAL_PACKAGE_NAME := AmazingSettings
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true

LOCAL_PROGUARD_ENABLED := disabled
include $(BUILD_PACKAGE)

include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES :=iwds-service-ui-jar:libs/iwds-service-ui-jar.jar
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES +=iwds-ui-with-res-jar:libs/iwds-ui-with-res-jar.jar
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES +=zxing-jar:libs/zxing.jar
include $(BUILD_MULTI_PREBUILT)

# Use the folloing include to make our test apk.
include $(call all-makefiles-under,$(LOCAL_PATH))
