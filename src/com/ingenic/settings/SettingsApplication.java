/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.settings.widget.FindMobilePhoneModel;

import android.app.Application;

public class SettingsApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        IwdsLog.d("tag", "appclication oncrate");
        FindMobilePhoneModel.getInstance(this).startDataTransactor();
    }
}