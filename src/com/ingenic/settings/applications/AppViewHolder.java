/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings.applications;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingenic.settings.R;

// View Holder used when displaying views
public class AppViewHolder {
    public ApplicationsState.AppEntry entry;
    public View rootView;
    public TextView appName;
    public ImageView appIcon;
    public TextView appSize;
    public TextView disabled;
    public CheckBox checkBox;

    static public AppViewHolder createOrRecycle(LayoutInflater inflater, View convertView) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.manage_applications_item, null);

            // Creates a ViewHolder and store references to the two children
            // views
            // we want to bind data to.
            AppViewHolder holder = new AppViewHolder();
            holder.rootView = convertView;
            holder.appName = (TextView) convertView.findViewById(R.id.app_name);
            holder.appIcon = (ImageView) convertView.findViewById(R.id.app_icon);
            holder.appSize = (TextView) convertView.findViewById(R.id.app_size);
            holder.disabled = (TextView) convertView.findViewById(R.id.app_disabled);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.app_on_sdcard);
            convertView.setTag(holder);
            return holder;
        } else {
            // Get the ViewHolder back to get fast access to the TextView
            // and the ImageView.
            return (AppViewHolder) convertView.getTag();
        }
    }

    void updateSizeText(CharSequence invalidSizeStr, int whichSize) {
        // if (ManageApplications.DEBUG) Log.i(ManageApplications.TAG,
        // "updateSizeText of " + entry.label + " " + entry
        // + ": " + entry.sizeStr);
        if (entry.sizeStr != null) {
            switch (whichSize) {
            case 1:// ManageApplications.SIZE_INTERNAL:
                appSize.setText(entry.internalSizeStr);
                break;
            case 2:// ManageApplications.SIZE_EXTERNAL:
                appSize.setText(entry.externalSizeStr);
                break;
            default:
                appSize.setText(entry.sizeStr);
                break;
            }
        } else if (entry.size == ApplicationsState.SIZE_INVALID) {
            appSize.setText(invalidSizeStr);
        }
    }
}