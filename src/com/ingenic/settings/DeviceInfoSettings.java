/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.XmlResourceParser;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.ingenic.iwds.widget.AdapterView;
import com.ingenic.iwds.widget.AmazingFocusListView;
import com.ingenic.iwds.widget.CircleImageView;

public class DeviceInfoSettings extends Fragment implements AdapterView.OnItemClickListener {

    private static final int TAPS_TO_BE_A_DEVELOPER = 7;

    private AmazingFocusListView mListView;
    private DeviceInfoAdapter mAdapter;
    private int mDevHitCountdown;
    private Toast mDevHitToast;
    private boolean mScreenisRound = false;

    public DeviceInfoSettings() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView;
        if (mScreenisRound) {
            rootView = inflater.inflate(R.layout.fragment_list_round, container, false);
        } else {
            rootView = inflater.inflate(R.layout.fragment_list_round, container, false);
        }
        mListView = (AmazingFocusListView) rootView.findViewById(R.id.list);
        mListView.addHeaderView(createTitleView(R.string.title_about));
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
        return rootView;
    }

    private View createTitleView(int titleRes) {
        TextView view = new TextView(getActivity());
        view.setGravity(Gravity.CENTER);
        view.setTextAppearance(getActivity(), android.R.style.TextAppearance_Large);
        view.setText(titleRes);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mScreenisRound = Utils.IsCircularScreen();
        if (mScreenisRound) {
            mAdapter = new DeviceInfoAdapter(activity, R.layout.header_normal_round, parserHeaders(activity));
        } else {
            mAdapter = new DeviceInfoAdapter(activity, R.layout.header_normal, parserHeaders(activity));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mDevHitCountdown = getActivity().getSharedPreferences(DevelopmentSettings.PREF_FILE,
                Context.MODE_PRIVATE).getBoolean(DevelopmentSettings.PREF_SHOW,
                android.os.Build.TYPE.equals("eng")) ? -1 : TAPS_TO_BE_A_DEVELOPER;
        mDevHitToast = null;
    }

    private List<Header> parserHeaders(Context context) {
        XmlResourceParser parser = context.getResources().getXml(R.xml.device_settings);

        List<Header> headers = new ArrayList<Header>();
        try {
            int eventType = parser.getEventType();

            while (eventType != XmlResourceParser.END_DOCUMENT) {
                switch (eventType) {
                case XmlResourceParser.START_TAG:
                    if ("header".equals(parser.getName())) {
                        Header header = parserHeader(parser);
                        if (header != null) {
                            headers.add(header);
                        }
                    }
                    break;
                }
                eventType = parser.next();
            }
        } catch (Exception e) {}
        return headers;
    }

    private Header parserHeader(XmlResourceParser parser) {
        Header header = new Header();

        int count = parser.getAttributeCount();
        for (int i = 0; i < count; i++) {
            String attr = parser.getAttributeName(i);
            int id = parser.getAttributeResourceValue(i, 0);

            if ("id".equals(attr)) {
                header.id = id;
            } else if ("title".equals(attr)) {
                if (id == 0) {
                    header.title = parser.getAttributeValue(i);
                } else {
                    header.title = getString(id);
                }
            } else if ("summary".equals(attr)) {
                if (id == 0) {
                    header.summary = parser.getAttributeValue(i);
                } else {
                    header.summary = getString(id);
                }
            } else if ("fragment".equals(attr)) {
                header.fragment = parser.getAttributeValue(i);
            }
        }

        return header;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        onHeaderClick(position);
    }

    private void onHeaderClick(int position) {
        Header header = mAdapter.getItem(position);
        if (header == null) return;

        if (header.fragment != null) {
            startFragment(header.fragment);
        } else {
            if (header.id == R.id.build_number) {
                if (UserHandle.myUserId() != UserHandle.USER_OWNER) return;

                if (mDevHitCountdown > 0) {
                    mDevHitCountdown--;
                    if (mDevHitCountdown == 0) {
                        getActivity()
                                .getSharedPreferences(DevelopmentSettings.PREF_FILE,
                                        Context.MODE_PRIVATE).edit()
                                .putBoolean(DevelopmentSettings.PREF_SHOW, true).apply();

                        if (mDevHitToast != null) {
                            mDevHitToast.cancel();
                        }

                        mDevHitToast = Toast.makeText(getActivity(), R.string.show_dev_on,
                                Toast.LENGTH_LONG);
                        mDevHitToast.show();
                    } else if (mDevHitCountdown > 0
                            && mDevHitCountdown < (TAPS_TO_BE_A_DEVELOPER - 2)) {
                        if (mDevHitToast != null) {
                            mDevHitToast.cancel();
                        }

                        mDevHitToast = Toast.makeText(
                                getActivity(),
                                getResources().getQuantityString(R.plurals.show_dev_countdown,
                                        mDevHitCountdown, mDevHitCountdown), Toast.LENGTH_SHORT);

                        mDevHitToast.show();
                    }
                } else if (mDevHitCountdown < 0) {
                    if (mDevHitToast != null) {
                        mDevHitToast.cancel();
                    }

                    mDevHitToast = Toast.makeText(getActivity(), R.string.show_dev_already,
                            Toast.LENGTH_LONG);
                    mDevHitToast.show();
                }
            }
        }
    }

    private void startFragment(String fragmentClass) {
        Intent it = new Intent(getActivity(), SubSettings.class);
        it.putExtra("fragment", fragmentClass);
        startActivity(it);
    }

    private static class DeviceInfoAdapter extends ArrayAdapter<Header> {
        private Context mContext;
        private int mResource;

        public DeviceInfoAdapter(Context context, int resource, List<Header> objects) {
            super(context, resource, objects);
            mContext = context;
            mResource = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(mResource, parent, false);

                holder = new ViewHolder();
                holder.icon = (CircleImageView) convertView.findViewById(android.R.id.icon);
                holder.title = (TextView) convertView.findViewById(R.id.title);
                holder.summary = (TextView) convertView.findViewById(R.id.summary);
                holder.icon.setCircleType(CircleImageView.TYPE_STROKE);
                holder.icon.setImageResource(R.drawable.ic_about);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Header header = getItem(position);
            header.summary = getSummary(header.id);
            holder.title.setText(header.title);

            if (header.summary == null) {
                holder.summary.setVisibility(View.GONE);
            } else {
                holder.summary.setVisibility(View.VISIBLE);
                holder.summary.setText(header.summary);
            }
            return convertView;
        }

        private String getSummary(int id) {
            switch (id) {
            case R.id.model_number:
                return Build.MODEL;
            case R.id.build_number:
                return Build.DISPLAY;
            case R.id.platform:
                return Build.BRAND + " " + SystemProperties.get("ro.board.platform");
            }
            return null;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            Header header = getItem(position);
            if (header.fragment != null) return true;
            if (header.id == R.id.build_number) return true;
            return false;
        }

        private static class ViewHolder {
            CircleImageView icon;
            TextView title;
            TextView summary;
        }
    }

    private static class Header {
        int id;
        CharSequence title;
        CharSequence summary;
        String fragment;
    }
}