/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * WuZhiMing(Mike)<zhiming.wu@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 675 Mass
 * Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class GestureEnabler extends Enabler {
    //the GestureService will listen the preference of the TAG. should not change it.
    private final static String TAG = "Gesture";
    private final static String GESTURE_STATE = "gesture_on";

    private Context mcontext;

    public GestureEnabler(Context context) {
        mcontext = context;
    }

    @Override
    public void setEnable(boolean enabled) {
        setChecked(enabled);
        performStatusChange(enabled);
    }

    @Override
    public String getSummary(Context context) {
        int state = getIntPref(mcontext, GESTURE_STATE, 0);
        switch(state) {
        case 0:
            return mcontext.getString(R.string.summary_close);
        case 1:
            return mcontext.getString(R.string.gesture_right);
        case 2:
            return mcontext.getString(R.string.gesture_left);
        default:
            return mcontext.getString(R.string.summary_close);
        }
    }

    @Override
    public boolean isEnabled() {
        return getIntPref(mcontext, GESTURE_STATE, 0) > 0;
    }

    private static int getIntPref(Context context, String name, int def) {
        SharedPreferences prefs =
            context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        int ret =  prefs.getInt(name, def);
        return ret;
    }
    private static void setIntPref(Context context, String name, int value) {
        SharedPreferences prefs =
            context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        Editor ed = prefs.edit();
        ed.putInt(name, value);
        ed.commit();
    }
}
