/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.settings.widget;

import java.util.Hashtable;
import java.util.Random;

import android.graphics.Bitmap;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

/**
 * 工具类
 * 
 * @author ShiGuangHua(Kenny)
 * 
 */
public class WidgetUtils {

    private static String encodeAddress(String url) {
        if (url == null || url.length() == 0) {
            return url;
        }
        char[] cs = url.toCharArray();
        char[] keys = new char[cs.length];
        for (int i = 0; i < cs.length; i++) {
            keys[i] = randomChar(cs[i] - i);
            int tmp = cs[i] + keys[i];
            while (tmp > Byte.MAX_VALUE || keys[i] == '-' || tmp == '-') {
                keys[i] = randomChar(cs[i]);
                tmp = cs[i] + keys[i];
            }
            cs[i] = (char) tmp;
        }
        url = new String(cs);
        url += "-";
        url += new String(keys);
        return url;
    }

    private static char randomChar(int seed) {
        Random random = new Random(System.currentTimeMillis());
        int parentSeed = random.nextInt();
        random.setSeed(parentSeed + seed);
        return (char) random.nextInt(Byte.MAX_VALUE);
    }

    public static Bitmap createQRImage(String url) {
        try {
            if (url == null || "".equals(url) || url.length() < 1) {
                return null;
            }
            url = encodeAddress(url);
            Hashtable<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
            BitMatrix bitMatrix = new QRCodeWriter().encode(url,
                    BarcodeFormat.QR_CODE, WidgetConstants.SCREEN_WIDTH,
                    WidgetConstants.SCREEN_HEIGHT, hints);
            int[] pixels = new int[WidgetConstants.SCREEN_WIDTH
                    * WidgetConstants.SCREEN_HEIGHT];
            for (int y = 0; y < WidgetConstants.SCREEN_HEIGHT; y++) {
                for (int x = 0; x < WidgetConstants.SCREEN_WIDTH; x++) {
                    if (bitMatrix.get(x, y)) {
                        pixels[y * WidgetConstants.SCREEN_WIDTH + x] = 0xff000000;
                    } else {
                        pixels[y * WidgetConstants.SCREEN_WIDTH + x] = 0xffffffff;
                    }
                }
            }
            Bitmap bitmap = Bitmap.createBitmap(WidgetConstants.SCREEN_WIDTH,
                    WidgetConstants.SCREEN_HEIGHT, Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, WidgetConstants.SCREEN_WIDTH, 0, 0,
                    WidgetConstants.SCREEN_WIDTH, WidgetConstants.SCREEN_HEIGHT);
            return bitmap;
        } catch (WriterException e) {
            // e.printStackTrace();
        }
        return null;
    }

}
