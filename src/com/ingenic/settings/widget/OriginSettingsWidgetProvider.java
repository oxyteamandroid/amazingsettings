/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.settings.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.provider.Settings;
import android.widget.RemoteViews;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.settings.AirPlaneModeEnabler;
import com.ingenic.settings.R;

/**
 * 
 * @author ShiGuangHua(Kenny)
 * 
 */
public class OriginSettingsWidgetProvider extends AppWidgetProvider {

    private RemoteViews mRemoteViews;

    /**
     * 是否已经连网
     */
    private static boolean mIsConnected = false;

    /**
     * 飞行模式是否可用
     */
    private boolean mIsAirPlaneEnabled = false;

    /**
     * 蓝牙adapter
     */
    private BluetoothAdapter mBluetoothAdapter;

    private AudioManager mAudioManager;
    private int mMaxVolume;
    private int mCurrentVolume;

    private static int mLastBrightId;

    /**
     * 当前屏幕的亮度值：0-255
     */
    private int mScreenBrightness;

    private FindMobilePhoneModel mFindMobilePhoneModel;

    private boolean mIsFindMobilePhone = false;

    // onUpdate() 在更新 widget 时，被执行，
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
            int[] appWidgetIds) {
        setBluetoothAdapter();
        setAudioManager(context);
        // 获取系统最大音量
        mMaxVolume = mAudioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        // 每次 widget 被创建时，对应的将widget的id添加到set中
        mRemoteViews = getRemoteViews(context);
        setConnectState(context);
        // read the airplane mode setting
        mIsAirPlaneEnabled = getAirPlaneEnable(context);
        saveAirPlaneMode(mIsAirPlaneEnabled);
        setBluetoothState();
        setMuteState(context);

        setOnViewClick(context, R.id.ib_plane);
        setOnViewClick(context, R.id.ib_bluetooth);
        setOnViewClick(context, R.id.ib_volume);
        setOnViewClick(context, R.id.tv_brightness_low);
        setOnViewClick(context, R.id.tv_brightness_middle);
        setOnViewClick(context, R.id.tv_brightness_high);
        setOnViewClick(context, R.id.iv_phone);

        mScreenBrightness = getScreenBrightness(context);
        setBrightResource(getIndexByBrightness(mScreenBrightness));

        for (int i = 0; i < appWidgetIds.length; i++) {
            appWidgetManager.updateAppWidget(appWidgetIds[i], mRemoteViews);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onEnabled(Context context) {
        IwdsLog.d("tag", "onEnable");
        mFindMobilePhoneModel = FindMobilePhoneModel.getInstance(context
                .getApplicationContext());
        mFindMobilePhoneModel.startDataTransactor();
        super.onEnabled(context);
    }

    private RemoteViews getRemoteViews(Context context) {
        mRemoteViews = new RemoteViews(context.getPackageName(),
                R.layout.widget_settings);

        return mRemoteViews;
    }

    private void setOnViewClick(Context context, int viewId) {
        Intent intent = new Intent(WidgetConstants.VIEW_CLICK_ACTION);
        intent.setClass(context, OriginSettingsWidgetProvider.class);
        intent.addCategory(Intent.CATEGORY_ALTERNATIVE);
        intent.setData(Uri.parse("custom:" + viewId));
        PendingIntent mPendingIntent = PendingIntent.getBroadcast(context, 0,
                intent, 1);
        mRemoteViews.setOnClickPendingIntent(viewId, mPendingIntent);
    }

    // 接收广播的回调函数
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        mRemoteViews = getRemoteViews(context);
        mScreenBrightness = getScreenBrightness(context);
        setBrightResource(getIndexByBrightness(mScreenBrightness));
        if (WidgetConstants.COM_INGENIC_CONNECT_ACTION.equals(action)) {
            // connected state
            mIsConnected = intent.getBooleanExtra(WidgetConstants.CONNECT_FLAG,
                    false);
            setConnectState(context);
            updateAllWidgets(context);
        } else if (Intent.ACTION_AIRPLANE_MODE_CHANGED.equals(action)) {
            // air plane change
            boolean airPlane = intent.getBooleanExtra("state", false);
            saveAirPlaneMode(airPlane);
            updateAllWidgets(context);
        } else if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
            // blue changed state
            setConnectState(context);
            setBluetoothState();
            updateAllWidgets(context);
        } else if (WidgetConstants.VOLUME_CHANGE_ACTION.equals(action)) {
            // change volume state
            setMuteState(context);
        } else if (intent.hasCategory(Intent.CATEGORY_ALTERNATIVE)) {
            Uri data = intent.getData();
            int viewId = Integer.parseInt(data.getSchemeSpecificPart());
            setClickEvent(context, viewId);
        }
        super.onReceive(context, intent);
    }

    /**
     * Updates the widget when something changes, or when a button is pushed.
     *
     * @param context
     */
    public void updateAllWidgets(Context context) {
        final AppWidgetManager appWidgetManager = AppWidgetManager
                .getInstance(context);
        appWidgetManager.updateAppWidget(new ComponentName(context,
                OriginSettingsWidgetProvider.class), mRemoteViews);
    }

    private void setClickEvent(Context context, int clickId) {
        switch (clickId) {
        case R.id.ib_bluetooth:
            setBluetoothAdapter();
            if (!mBluetoothAdapter.isEnabled()) {
                mBluetoothAdapter.enable();
            } else {
                mBluetoothAdapter.disable();
            }
            break;
        case R.id.ib_plane:
            mIsAirPlaneEnabled = getAirPlaneEnable(context);
            mIsAirPlaneEnabled = !mIsAirPlaneEnabled;
            Settings.Global.putInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, mIsAirPlaneEnabled ? 1
                            : 0);
            Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
            intent.putExtra("state", mIsAirPlaneEnabled);
            context.sendBroadcast(intent);

            break;
        case R.id.ib_volume:
            setAudioManager(context);
            // 获取系统最大音量
            mMaxVolume = mAudioManager
                    .getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            mCurrentVolume = mAudioManager
                    .getStreamVolume(AudioManager.STREAM_MUSIC);
            if (mCurrentVolume > 0) {
                // 非静音状态，点击变为静音状态
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
                mCurrentVolume = 0;
            } else {
                // 静音状态，点击变为非静音状态
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        mMaxVolume * 8 / 10, 0);
                mCurrentVolume = mMaxVolume * 8 / 10;
            }
            break;

        case R.id.tv_brightness_low:
        case R.id.tv_brightness_middle:
        case R.id.tv_brightness_high:
            saveScreenBrightness(context, getBrightValueByViewId(clickId));
            setBrightResource(clickId);
            updateAllWidgets(context);
            break;
        case R.id.iv_phone:

            setBluetoothAdapter();
            if (mFindMobilePhoneModel == null)
                mFindMobilePhoneModel = FindMobilePhoneModel
                        .getInstance(context.getApplicationContext());

            if (mBluetoothAdapter.isEnabled()) {
                if (mFindMobilePhoneModel != null && mIsConnected) {
                    mFindMobilePhoneModel.sendDataTransactor("startFindMobile");
                }
            }
            break;
        default:
            break;
        }
    }

    private void setBluetoothAdapter() {
        if (mBluetoothAdapter == null)
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    private void setAudioManager(Context context) {
        if (mAudioManager == null) {
            mAudioManager = (AudioManager) context
                    .getSystemService(Context.AUDIO_SERVICE);
        }
    }

    /**
     * 设置静音状态
     */
    private void setMuteState(Context context) {

        setAudioManager(context);
        mCurrentVolume = mAudioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC);
        if (mCurrentVolume > 0) {
            mRemoteViews.setImageViewResource(R.id.ib_volume,
                    R.drawable.widget_volume_selector);
        } else {
            mRemoteViews.setImageViewResource(R.id.ib_volume,
                    R.drawable.widget_volume_selected);
        }
        updateAllWidgets(context);
    }

    /**
     * 设置蓝牙状态
     */
    private void setBluetoothState() {
        if (mBluetoothAdapter != null) {
            if (!mBluetoothAdapter.isEnabled()) {
                mRemoteViews.setImageViewResource(R.id.ib_bluetooth,
                        R.drawable.widget_bluetooth_selector);
            } else {
                mRemoteViews.setImageViewResource(R.id.ib_bluetooth,
                        R.drawable.widget_bluetooth_selected);
            }
        }
    }

    /**
     * 设置蓝牙链接状态
     */
    private void setConnectState(Context context) {
        setBluetoothAdapter();
        if (mBluetoothAdapter.isEnabled() && mIsConnected) {
            // 蓝牙已经链接
            mRemoteViews.setTextColor(R.id.tv_internet_state, Color.WHITE);
            mRemoteViews.setTextViewText(R.id.tv_internet_state,
                    context.getString(R.string.connect_with_phone));
            mRemoteViews.setOnClickPendingIntent(R.id.tv_internet_state, null);

        } else {
            // 已断开链接
            mRemoteViews.setTextColor(R.id.tv_internet_state,
                    Color.rgb(255, 29, 31));
            mRemoteViews.setTextViewText(R.id.tv_internet_state,
                    context.getString(R.string.disconnect_with_phone));

            Intent intent = new Intent(WidgetConstants.GO_QR_ACTIVITY_ACTION);
            mRemoteViews.setOnClickPendingIntent(R.id.tv_internet_state,
                    PendingIntent.getActivity(context, 0, intent, 0));
        }
    }

    /**
     * 设置亮度状态
     * 
     * @param current
     */
    private void setBrightResource(int current) {
        if (mLastBrightId != 0) {
            mRemoteViews.setInt(mLastBrightId, "setBackgroundResource",
                    R.drawable.widget_bg_gay);
        }

        mRemoteViews.setInt(current, "setBackgroundResource",
                R.drawable.widget_bg_red);
        mLastBrightId = current;

    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
    }

    /**
     * 根据亮度获取viewId
     * 
     * @param bright
     * @return
     */
    private int getIndexByBrightness(int bright) {
        if (mScreenBrightness >= 0 && mScreenBrightness < 100) {
            return R.id.tv_brightness_low;
        } else if (mScreenBrightness >= 100 && mScreenBrightness < 200) {
            return R.id.tv_brightness_middle;
        } else if (mScreenBrightness >= 200 && mScreenBrightness <= 255) {
            return R.id.tv_brightness_high;
        }

        return R.id.tv_brightness_low;
    }

    /**
     * 得到飞行模式的状态
     * 
     * @param context
     * @return
     */
    private boolean getAirPlaneEnable(Context context) {
        return AirPlaneModeEnabler.isAirplaneModeOn(context);
    }

    /**
     * 设置当前飞行模式的状态
     */
    private void saveAirPlaneMode(boolean enabled) {

        if (enabled) {
            // 飞行模式可用
            mRemoteViews.setImageViewResource(R.id.ib_plane,
                    R.drawable.widget_plane_selected);
        } else {
            // 飞行模式不可用
            mRemoteViews.setImageViewResource(R.id.ib_plane,
                    R.drawable.widget_plane_selector);
        }
    }

    /**
     * 通过viewId 获取屏幕亮度
     * 
     * @param id
     * @return
     */
    private int getBrightValueByViewId(int id) {
        switch (id) {
        case R.id.tv_brightness_low:
            return WidgetConstants.LOW_SCREEN_BRIGHTNESS;
        case R.id.tv_brightness_middle:
            return WidgetConstants.MIDDLE_SCREEN_BRIGHTNESS;
        case R.id.tv_brightness_high:
            return WidgetConstants.HIGH_SCREEN_BRIGHTNESS;
        }
        return WidgetConstants.LOW_SCREEN_BRIGHTNESS;
    }

    /**
     * 获得当前屏幕亮度值 0--255
     */
    private int getScreenBrightness(Context context) {
        int screenBrightness = WidgetConstants.HIGH_MAX_BRIGHTNESS;
        try {
            screenBrightness = Settings.System.getInt(
                    context.getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS);
        } catch (Exception localException) {
            localException.printStackTrace();
        }
        return screenBrightness;
    }

    /**
     * 设置当前屏幕亮度值 0--255
     */
    private void saveScreenBrightness(Context context, int paramInt) {
        try {
            Settings.System.putInt(context.getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS, paramInt);
        } catch (Exception localException) {
            localException.printStackTrace();
        }
    }

}
