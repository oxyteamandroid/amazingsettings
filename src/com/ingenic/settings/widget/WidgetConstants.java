/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.settings.widget;

/**
 * 常量
 * 
 * @author ShiGuangHua(Kenny)
 * 
 */
public class WidgetConstants {
    /**
     * 屏幕的宽度和高度，这个是动态设置的，可以适配其它屏幕
     */
    public static int SCREEN_WIDTH = 320, SCREEN_HEIGHT = 320;

    /**
     * iphone链接状态action
     */
    public static final String ANCS_CONNECTED_STATE = "com.indroid.appleconnectstate";

    /**
     * iphone 链接 flag
     */
    public static final String ANCS_CONNECT_FLAG = "connState";

    /**
     * iphone 链接
     */
    public static final String ANCS_CONNECT_STATE = "connected";

    /**
     * iphone 断开
     */
    public static final String ANCS_DISCONNECT_STATE = "disconnected";

    /**
     * 跳转到二维码界面的action
     */
    public static final String GO_QR_ACTIVITY_ACTION = "com.ingenic.action.qr.activity";

    /**
     * 手机与手表链接状态的action
     */
    public static final String COM_INGENIC_CONNECT_ACTION = "com.ingenic.setting.channel.avaiable";

    /**
     * 手表端控制手机端，手机端响铃声
     */
    public static final String COM_INGENIC_MOBILE_START_RING = "mobileStartRing";

    /**
     * 手表端控制手机端，手机端停止铃声
     */
    public static final String COM_INGENIC_MOBILE_STOP_RING = "mobileStopRing";

    /**
     * 手机端控制手表端，手表开始铃声
     */
    public static final String COM_INGENIC_MOBILE_FIND_WATCH_RING = "findWatch";

    /**
     * 链接标志
     */
    public static final String CONNECT_FLAG = "connect_state";

    /**
     * 音量变化的广播
     */
    public static final String VOLUME_CHANGE_ACTION = "android.media.VOLUME_CHANGED_ACTION";

    /**
     * view的点击事件
     */
    public static final String VIEW_CLICK_ACTION = "widget_view_click_action";


    /**
     * 亮度值： 低
     */
    public static final int LOW_SCREEN_BRIGHTNESS = 50;

    /**
     * 亮度值： 中
     */
    public static final int MIDDLE_SCREEN_BRIGHTNESS = 150;

    /**
     * 亮度值： 高
     */
    public static final int HIGH_SCREEN_BRIGHTNESS = 250;

    /**
     * 亮度值： 最大亮度
     */
    public static final int HIGH_MAX_BRIGHTNESS = 255;

    /**
     * 提示用户打开手表端蓝牙的flag
     */
    public static final String OPEN_BLUETOOTH_FLAG = "openbluetooth";

    /**
     * 蓝牙地址的flag
     */
    public static final String BLUETOOTCH_ADDRESS = "address";

    /**
     * Preferences 的flag
     */
    public static final String PREFERENCES_BLUETADDRESS = "blueAddress";

}