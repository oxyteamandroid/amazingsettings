/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.settings.widget;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.AudioManager;
import android.provider.Settings;

import com.ingenic.iwds.appwidget.WidgetProvider;
import com.ingenic.iwds.widget.LocalRemoteViews;
import com.ingenic.iwds.widget.OnClickHandler;
import com.ingenic.settings.AirPlaneModeEnabler;
import com.ingenic.settings.R;

/**
 * 
 * @author ShiGuangHua(Kenny)
 * 
 */
public class SettingsWidgetProvider extends WidgetProvider {

    private LocalRemoteViews mRemoteViews;

    /**
     * 是否已经连网
     */
    private boolean mIsConnected = false;

    /**
     * 飞行模式是否可用
     */
    private boolean mIsAirPlaneEnabled = false;

    /**
     * 蓝牙adapter
     */
    private BluetoothAdapter mBluetoothAdapter;

    private AudioManager mAudioManager;
    private int mMaxVolume;
    private int mCurrentVolume;

    private int mLastBrightId;

    /**
     * 当前屏幕的亮度值：0-255
     */
    private int mScreenBrightness;

    private PendingIntent mPendIngIntent;

    private FindMobilePhoneModel mFindMobilePhoneModel;

    private boolean mIsFindMobilePhone = false;

    private SettingWidgetReceiver mReceiver;

    private Context mContext;

    private class SettingWidgetReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (WidgetConstants.COM_INGENIC_CONNECT_ACTION.equals(action)) {
                // connected state
                mIsConnected = intent.getBooleanExtra(
                        WidgetConstants.CONNECT_FLAG, false);
                setConnectState();
            } else if (Intent.ACTION_AIRPLANE_MODE_CHANGED.equals(action)) {
                // air plane change
                boolean airPlane = intent.getBooleanExtra("state", false);
                saveAirPlaneMode(airPlane);
            } else if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                // blue changed state
                setConnectState();
                setBluetoothState();
            } else if (WidgetConstants.VOLUME_CHANGE_ACTION.equals(action)) {
                // change volume state
                setMuteState();
            }
            updateAllWidgets(mRemoteViews);
        }

    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        // 获取系统最大音量
        mMaxVolume = mAudioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        mFindMobilePhoneModel = FindMobilePhoneModel
                .getInstance(getApplicationContext());
        mFindMobilePhoneModel.startDataTransactor();

        IntentFilter filter = new IntentFilter();
        // 手机与手表链接的action
        filter.addAction(WidgetConstants.COM_INGENIC_CONNECT_ACTION);
        // 飞行模式的action
        filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        // 蓝牙链接的action
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        // 音量改变的action
        filter.addAction(WidgetConstants.VOLUME_CHANGE_ACTION);
        mReceiver = new SettingWidgetReceiver();
        // 注册广播
        getApplicationContext().registerReceiver(mReceiver, filter);
    }

    @Override
    protected LocalRemoteViews onCreateRemoteViews() {

        if (mRemoteViews == null) {
            mRemoteViews = new LocalRemoteViews(getPackageName(),
                    R.layout.widget_settings);
        }

        setConnectedPendIngIntent();
        setConnectState();
        // read the airplane mode setting
        mIsAirPlaneEnabled = getAirPlaneEnable(mContext);
        saveAirPlaneMode(mIsAirPlaneEnabled);
        setBluetoothState();
        setMuteState();

        mRemoteViews.setOnClickHandler(R.id.ib_plane, mOnClickHandler);
        mRemoteViews.setOnClickHandler(R.id.ib_bluetooth, mOnClickHandler);
        mRemoteViews.setOnClickHandler(R.id.ib_volume, mOnClickHandler);
        mRemoteViews.setOnClickHandler(R.id.tv_brightness_low, mOnClickHandler);
        mRemoteViews.setOnClickHandler(R.id.tv_brightness_middle,
                mOnClickHandler);
        mRemoteViews
                .setOnClickHandler(R.id.tv_brightness_high, mOnClickHandler);
        mRemoteViews.setOnClickHandler(R.id.iv_phone, mOnClickHandler);

        mScreenBrightness = getScreenBrightness();
        setBrightResource(getIndexByBrightness(mScreenBrightness));

        return mRemoteViews;
    }

    /**
     * 设置静音状态
     */
    private void setMuteState() {
        mCurrentVolume = mAudioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC);
        if (mCurrentVolume > 0) {
            mRemoteViews.setImageViewResource(R.id.ib_volume,
                    R.drawable.widget_volume_selector);
        } else {
            mRemoteViews.setImageViewResource(R.id.ib_volume,
                    R.drawable.widget_volume_selected);
        }
    }

    /**
     * 设置链接状态的点击事件
     */
    private void setConnectedPendIngIntent() {
        Intent intent = new Intent(WidgetConstants.GO_QR_ACTIVITY_ACTION);
        mPendIngIntent = PendingIntent.getActivity(mContext, 0, intent, 0);
    }

    /**
     * 设置蓝牙状态
     */
    private void setBluetoothState() {
        if (mBluetoothAdapter != null) {
            if (!mBluetoothAdapter.isEnabled()) {
                mRemoteViews.setImageViewResource(R.id.ib_bluetooth,
                        R.drawable.widget_bluetooth_selector);
            } else {
                mRemoteViews.setImageViewResource(R.id.ib_bluetooth,
                        R.drawable.widget_bluetooth_selected);
            }
        }
    }

    /**
     * 设置蓝牙链接状态
     */
    private void setConnectState() {
        if (mBluetoothAdapter == null)
            return;
        if (mBluetoothAdapter.isEnabled() && mIsConnected) {
            // 蓝牙已经链接
            mRemoteViews.setTextColor(R.id.tv_internet_state, Color.WHITE);
            mRemoteViews.setTextViewText(R.id.tv_internet_state,
                    getString(R.string.connect_with_phone));
            mRemoteViews.setOnClickPendingIntent(R.id.tv_internet_state, null);

        } else {
            // 已断开链接
            mRemoteViews.setTextColor(R.id.tv_internet_state,
                    Color.rgb(255, 29, 31));
            mRemoteViews.setTextViewText(R.id.tv_internet_state,
                    getString(R.string.disconnect_with_phone));
            mRemoteViews.setOnClickPendingIntent(R.id.tv_internet_state,
                    mPendIngIntent);
        }
    }

    /**
     * 设置亮度状态
     * 
     * @param current
     */
    private void setBrightResource(int current) {
        if (current == mLastBrightId)
            return;
        if (mLastBrightId != 0) {
            mRemoteViews.setInt(mLastBrightId, "setBackgroundResource",
                    R.drawable.widget_bg_gay);
        }

        mRemoteViews.setInt(current, "setBackgroundResource",
                R.drawable.widget_bg_red);
        mLastBrightId = current;

    }

    /**
     * 根据亮度获取viewId
     * 
     * @param bright
     * @return
     */
    private int getIndexByBrightness(int bright) {
        if (mScreenBrightness >= 0 && mScreenBrightness < 100) {
            return R.id.tv_brightness_low;
        } else if (mScreenBrightness >= 100 && mScreenBrightness < 200) {
            return R.id.tv_brightness_middle;
        } else if (mScreenBrightness >= 200 && mScreenBrightness <= 255) {
            return R.id.tv_brightness_high;
        }

        return R.id.tv_brightness_low;
    }

    private OnClickHandler mOnClickHandler = new OnClickHandler() {

        @SuppressLint("NewApi")
        @Override
        protected void onClick(String callingPkg, int viewId) {
            // TODO Auto-generated method stub
            switch (viewId) {
            case R.id.ib_bluetooth:
                if (mBluetoothAdapter == null)
                    return;
                if (!mBluetoothAdapter.isEnabled()) {
                    mBluetoothAdapter.enable();
                } else {
                    mBluetoothAdapter.disable();
                }
                break;
            case R.id.ib_plane:
                mIsAirPlaneEnabled = getAirPlaneEnable(mContext);
                mIsAirPlaneEnabled = !mIsAirPlaneEnabled;
                Settings.Global.putInt(mContext.getContentResolver(),
                        Settings.Global.AIRPLANE_MODE_ON,
                        mIsAirPlaneEnabled ? 1 : 0);
                Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
                intent.putExtra("state", mIsAirPlaneEnabled);
                getApplicationContext().sendBroadcast(intent);

                break;
            case R.id.ib_volume:
                if (mCurrentVolume > 0) {
                    // 非静音状态，点击变为静音状态
                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0,
                            0);
                    mCurrentVolume = 0;
                } else {
                    // 静音状态，点击变为非静音状态
                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                            mMaxVolume * 8 / 10, 0);
                    mCurrentVolume = mMaxVolume * 8 / 10;
                }
                break;

            case R.id.tv_brightness_low:
            case R.id.tv_brightness_middle:
            case R.id.tv_brightness_high:
                saveScreenBrightness(getBrightValueByViewId(viewId));
                setBrightResource(viewId);
                updateWidgetForHost(callingPkg, mRemoteViews);
                break;
            case R.id.iv_phone:

                if (mBluetoothAdapter == null)
                    return;

                if (mBluetoothAdapter.isEnabled()) {
                    if (mFindMobilePhoneModel != null && mIsConnected) {
                        mFindMobilePhoneModel
                        .sendDataTransactor("startFindMobile");
                    }
                }
                break;

            default:
                break;
            }
        }
    };

    /**
     * 得到飞行模式的状态
     * 
     * @param context
     * @return
     */
    private boolean getAirPlaneEnable(Context context) {
        return AirPlaneModeEnabler.isAirplaneModeOn(context);
    }

    /**
     * 设置当前飞行模式的状态
     */
    private void saveAirPlaneMode(boolean enabled) {

        if (enabled) {
            // 飞行模式可用
            mRemoteViews.setImageViewResource(R.id.ib_plane,
                    R.drawable.widget_plane_selected);
        } else {
            // 飞行模式不可用
            mRemoteViews.setImageViewResource(R.id.ib_plane,
                    R.drawable.widget_plane_selector);
        }
    }

    /**
     * 通过viewId 获取屏幕亮度
     * 
     * @param id
     * @return
     */
    private int getBrightValueByViewId(int id) {
        switch (id) {
        case R.id.tv_brightness_low:
            return WidgetConstants.LOW_SCREEN_BRIGHTNESS;
        case R.id.tv_brightness_middle:
            return WidgetConstants.MIDDLE_SCREEN_BRIGHTNESS;
        case R.id.tv_brightness_high:
            return WidgetConstants.HIGH_SCREEN_BRIGHTNESS;
        }
        return WidgetConstants.LOW_SCREEN_BRIGHTNESS;
    }

    /**
     * 获得当前屏幕亮度值 0--255
     */
    private int getScreenBrightness() {
        int screenBrightness = WidgetConstants.HIGH_MAX_BRIGHTNESS;
        try {
            screenBrightness = Settings.System.getInt(getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS);
        } catch (Exception localException) {
            localException.printStackTrace();
        }
        return screenBrightness;
    }

    /**
     * 设置当前屏幕亮度值 0--255
     */
    private void saveScreenBrightness(int paramInt) {
        try {
            Settings.System.putInt(getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS, paramInt);
        } catch (Exception localException) {
            localException.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        // 注销广播
        if (mReceiver != null) {
            getApplicationContext().unregisterReceiver(mReceiver);
        }
        super.onDestroy();
    }

}
