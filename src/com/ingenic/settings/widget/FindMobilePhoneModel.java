/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.settings.widget;

import android.content.Context;
import android.content.Intent;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactorCallback;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.settings.FindWatchActivity;

/**
 * 
 * @author ShiGuangHua(Kenny)
 * 
 */
public class FindMobilePhoneModel implements DataTransactorCallback {

    private static FindMobilePhoneModel mInstance = null;
    private static DataTransactor mDataTransactor = null;

    private Context mContext;
    private static final String FIND_PHONE_UUID = "D1E386E3-6BBF-40E0-BF84-E0C41F32B6E7";
    private static final String TAG = FindMobilePhoneModel.class.getSimpleName();

    public static FindMobilePhoneModel getInstance(Context context) {
        if (mInstance == null) {
            IwdsLog.d(TAG, "init FindMobilePhoneModel");
            mInstance = new FindMobilePhoneModel(context);
        }
        return mInstance;
    }

    public FindMobilePhoneModel(Context context) {
        mContext = context;
        if (mDataTransactor == null) {
            mDataTransactor = new DataTransactor(mContext, this,
                    FIND_PHONE_UUID);
        }
    }

    public void startDataTransactor() {
        if (mDataTransactor != null)
            mDataTransactor.start();
    }

    public void stopDataTransactor() {
        if (mDataTransactor != null)
            mDataTransactor.stop();
    }

    public void sendDataTransactor(String cmd) {
        if (mDataTransactor != null)
            mDataTransactor.send(cmd);
    }

    @Override
    public void onChannelAvailable(boolean available) {
        IwdsLog.d(TAG, "onChannelAvailable: " + available);
        // 发广播给SettingsWidgetProvider通知手机与手表的连接状态
        Intent intent = new Intent();
        intent.setAction(WidgetConstants.COM_INGENIC_CONNECT_ACTION);
        intent.putExtra(WidgetConstants.CONNECT_FLAG, available);
        mContext.sendBroadcast(intent);
    }

    @Override
    public void onDataArrived(Object arg0) {
        String cmd = (String) arg0;
        if (cmd.equals(WidgetConstants.COM_INGENIC_MOBILE_START_RING)) {
            // 手机正在响铃
            // Toast.makeText(mContext,
            // mContext.getString(R.string.mobileisRing),Toast.LENGTH_SHORT).show();
        } else if (cmd.equals(WidgetConstants.COM_INGENIC_MOBILE_STOP_RING)) {
            // 手机停止响铃
            // Toast.makeText(mContext,mContext.getString(R.string.mobileStopRing),Toast.LENGTH_SHORT).show();
        } else if (cmd.equals(WidgetConstants.COM_INGENIC_MOBILE_FIND_WATCH_RING)) {
            IwdsLog.d(TAG, "receive findWatch");
            FindWatchActivity.startFindActivity(mContext);
        }

    }

    @Override
    public void onLinkConnected(DeviceDescriptor arg0, boolean arg1) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onRecvFileProgress(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSendFileProgress(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSendResult(DataTransactResult arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onRecvFileInterrupted(int index) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSendFileInterrupted(int index) {
        // TODO Auto-generated method stub

    }

}
