/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingLauncher Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.settings.widget;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.widget.ImageView;

import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.widget.RightScrollView;
import com.ingenic.iwds.widget.RightScrollView.OnRightScrollListener;
import com.ingenic.settings.R;

/**
 * 二维码界面
 * 
 * @author ShiGuangHua(Kenny)
 * 
 */
public class WidgetQrActivity extends RightScrollActivity implements
        OnRightScrollListener {
    private ImageView mQrView;
    private RightScrollView mRightScrollView;
    private SharedPreferences mPreferenceBlue;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WidgetConstants.SCREEN_WIDTH = getWindowManager().getDefaultDisplay()
                .getWidth();
        WidgetConstants.SCREEN_HEIGHT = getWindowManager().getDefaultDisplay()
                .getHeight();

        mRightScrollView = getRightScrollView();
        mRightScrollView.disableRightScroll();
        mRightScrollView.setContentView(R.layout.widget_qrlayout);

        mQrView = (ImageView) findViewById(R.id.qrView);

        initQrView();
        IntentFilter mFilter = new IntentFilter();
        mFilter.addAction(WidgetConstants.COM_INGENIC_CONNECT_ACTION);
        mFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        mFilter.addAction(WidgetConstants.ANCS_CONNECTED_STATE);
        registerReceiver(mReceiver, mFilter);
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (WidgetConstants.COM_INGENIC_CONNECT_ACTION.equals(action)
                    || WidgetConstants.ANCS_CONNECTED_STATE.equals(action)) {
                boolean avaiable = intent.getBooleanExtra(
                        WidgetConstants.CONNECT_FLAG, false);
                boolean ancsAvaiable = intent
                        .getExtras()
                        .getString(WidgetConstants.ANCS_CONNECT_FLAG,
                                WidgetConstants.ANCS_DISCONNECT_STATE)
                        .equals(WidgetConstants.ANCS_CONNECT_STATE) ? true
                        : false;
                if (avaiable || ancsAvaiable) {
                    finish();
                }
            } else if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                finish();
            }
        }
    };

    private void initQrView() {
        mPreferenceBlue = this.getSharedPreferences(
                WidgetConstants.PREFERENCES_BLUETADDRESS, Context.MODE_PRIVATE);
        if (WidgetConstants.OPEN_BLUETOOTH_FLAG.equals(mPreferenceBlue
                .getString(WidgetConstants.BLUETOOTCH_ADDRESS,
                        WidgetConstants.OPEN_BLUETOOTH_FLAG))) {
            String blueAddress = BluetoothAdapter.getDefaultAdapter()
                    .getAddress();
            Editor edit = mPreferenceBlue.edit();
            edit.putString(WidgetConstants.BLUETOOTCH_ADDRESS, blueAddress);
            edit.commit();
        }
        if (BluetoothAdapter.getDefaultAdapter().getState() == BluetoothAdapter.STATE_ON) {
            mQrView.setImageBitmap(WidgetUtils.createQRImage(mPreferenceBlue
                    .getString(WidgetConstants.BLUETOOTCH_ADDRESS,
                            WidgetConstants.OPEN_BLUETOOTH_FLAG)));
        } else {
            mQrView.setImageBitmap(WidgetUtils
                    .createQRImage(WidgetConstants.OPEN_BLUETOOTH_FLAG));
        }
    }

    @Override
    public void onRightScroll() {
        finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        mRightScrollView.enableRightScroll();
    }

}