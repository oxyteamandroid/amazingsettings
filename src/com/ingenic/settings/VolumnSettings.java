/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings;

import com.ingenic.iwds.widget.AmazingFocusListView;
import com.ingenic.iwds.widget.CircleImageView;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class VolumnSettings extends Fragment implements AmazingFocusListView.OnFocusChangedListener {
    private AmazingFocusListView mListView;
    private VolumnAdapter mAdapter;
    private AudioManager mAudioManager;
    private boolean mScreenisRound = false;
    private Handler mHandler = new Handler();
    private SetVolumnRunnable mRunnable = new SetVolumnRunnable();

    private static final int[] STREAM_TYPE = new int[] { AudioManager.STREAM_MUSIC,
            AudioManager.STREAM_RING, AudioManager.STREAM_NOTIFICATION, AudioManager.STREAM_ALARM };

    public VolumnSettings() {}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mAudioManager = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
        String[] entries = activity.getResources().getStringArray(R.array.volumn_entries);
        int max = getMaxVolume();
        mScreenisRound = Utils.IsCircularScreen();
        if (mScreenisRound) {
            mAdapter = new VolumnAdapter(activity, R.layout.list_item_round, entries, max);
        } else {
            mAdapter = new VolumnAdapter(activity, R.layout.list_item, entries, max);
        }
    }

    private int getMaxVolume() {
        int max = 15;
        for (int type : STREAM_TYPE) {
            max = Math.min(max, mAudioManager.getStreamMaxVolume(type));
        }
        return max;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView;
        if (mScreenisRound) {
            rootView = inflater.inflate(R.layout.fragment_list_round, container,false);
        } else  {
            rootView = inflater.inflate(R.layout.fragment_list, container,false);
        }

        mListView = (AmazingFocusListView) rootView.findViewById(R.id.list);
        mListView.addHeaderView(createTitleView(R.string.title_volumn));
        mListView.addFooterView(new View(getActivity()));
        mListView.setAdapter(mAdapter);

        int volumn = mAudioManager.getStreamVolume(AudioManager.STREAM_RING);
        mListView.setFocusPosition(mAdapter.getCount() - volumn - 1);
        mListView.setOnFocusChangedListener(this);
        mListView.setFocusMode(AmazingFocusListView.FOCUS_MODE_CLICK);
        return rootView;
    }

    private View createTitleView(int titleRes) {
        TextView view = new TextView(getActivity());
        view.setGravity(Gravity.CENTER);
        view.setTextAppearance(getActivity(), android.R.style.TextAppearance_Large);
        view.setText(titleRes);
        return view;
    }

    private static class VolumnAdapter extends ArrayAdapter<CharSequence> {
        private Context mContext;
        private int mResource;
        private int mMax;

        @Override
        public int getCount() {
            return mMax + 1;
        }

        @Override
        public CharSequence getItem(int position) {
            int index = mMax - position;
            if (index == 0) {
                return mContext.getString(R.string.mute);
            }
            return String.valueOf(mMax - position);
        }

        public VolumnAdapter(Context context, int resource, CharSequence[] objects, int maxVolume) {
            super(context, resource, objects);
            mContext = context;
            mResource = resource;
            mMax = maxVolume;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(mResource, parent, false);

                holder = new ViewHolder();
                holder.icon = (CircleImageView) convertView.findViewById(android.R.id.icon);
                holder.icon.setImageResource(R.drawable.ic_volumn);
                holder.icon.setCircleType(CircleImageView.TYPE_STROKE);
                holder.text = (TextView) convertView.findViewById(R.id.text);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.text.setText(getItem(position));

            return convertView;
        }

        private static class ViewHolder {
            CircleImageView icon;
            TextView text;
        }
    }

    void setVolumn(int volumn) {
        int max = getMaxVolume();
        for (int type : STREAM_TYPE) {
            // if (volumn == 0) {
            // mAudioManager.setStreamMute(type, true);
            // } else {
            // mAudioManager.setStreamMute(type, false);
            // }
            int thisMax = mAudioManager.getStreamMaxVolume(type);
            int vol = volumn * thisMax / max;
            mAudioManager.setStreamVolume(type, vol, AudioManager.FLAG_PLAY_SOUND);
        }
    }

    @Override
    public void onFocusChanged(AmazingFocusListView view, final int focus, int oldFocus) {
        mHandler.removeCallbacks(mRunnable);
        mRunnable.excute(mAdapter.getCount() - focus - 1);
        mHandler.post(mRunnable);
    }

    private class SetVolumnRunnable implements Runnable {

        private int mVolumn;

        private void excute(int volumn) {
            mVolumn = volumn;
        }

        @Override
        public void run() {
            setVolumn(mVolumn);
        }
    }
}
