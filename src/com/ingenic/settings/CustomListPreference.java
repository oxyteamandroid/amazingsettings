/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.ListPreference;
import android.util.AttributeSet;

public class CustomListPreference extends ListPreference implements
        AmazingListDialog.ItemClickListner {

    private CharSequence[] mEntries;
    private CharSequence[] mEntryValues;
    private int mClickedDialogEntryIndex;
    private AmazingListDialog mDialog;
    private CharSequence mDialogTitle;

    public CustomListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs,
                com.android.internal.R.styleable.ListPreference, 0, 0);
        mEntries = a.getTextArray(com.android.internal.R.styleable.ListPreference_entries);
        mEntryValues = a.getTextArray(com.android.internal.R.styleable.ListPreference_entryValues);
        a.recycle();

        /*
         * Retrieve the Preference summary attribute since it's private in the Preference class.
         */
        a = context.obtainStyledAttributes(attrs,
                com.android.internal.R.styleable.DialogPreference, 0, 0);
        mDialogTitle = a.getString(com.android.internal.R.styleable.DialogPreference_dialogTitle);
        if (mDialogTitle == null) {
            // Fallback on the regular title of the preference
            // (the one that is seen in the list)
            mDialogTitle = getTitle();
        }
        a.recycle();
    }

    public CustomListPreference(Context context) {
        this(context, null);
    }

    @Override
    protected void showDialog(Bundle state) {
        Context context = getContext();

        mClickedDialogEntryIndex = findIndexOfValue(getValue());
        mDialog = new AmazingListDialog(context, mEntries, mClickedDialogEntryIndex);
        mDialog.setTitleText(mDialogTitle);
        mDialog.setItemClickListner(this);

        // Create the dialog
        if (state != null) {
            mDialog.onRestoreInstanceState(state);
        }

        mDialog.setOnDismissListener(this);
        mDialog.show();

    }

    @Override
    public void onItemClick(int position) {
        String value = mEntryValues[position].toString();
        callChangeListener(value);
        if (mDialog.isShowing()) mDialog.dismiss();
    }
}