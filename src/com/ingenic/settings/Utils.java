/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;

import com.ingenic.iwds.HardwareList;
import com.ingenic.iwds.utils.IwdsLog;

public class Utils {
    public static final String ACTION_RESTART_SERVICE = "ingenic.intent.action.RESTART_DMSERVICE";
    public static final String TAG = "AmazingSettings-Utils";
    public static final String SHARE_PREFERENCE = "AmazingSettings";
    public static final String UPDATE_ZIP_MD5 = "md5";
    public static final String DEFAULT_UPDATE_ZIP_MD5 = "unknown";
    public static final String default_updatefilename = "sdcard/iwds/update.zip";

    public static void setUpdatezipMD5(Context context, String md5) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCE, Context.MODE_PRIVATE);
        sp.edit().putString(UPDATE_ZIP_MD5, md5).commit();
    }

    public static String getUpdatezipMD5(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCE, Context.MODE_PRIVATE);
        return sp.getString(UPDATE_ZIP_MD5, DEFAULT_UPDATE_ZIP_MD5);
    }

    public static String getMd5ByFile(File file) throws FileNotFoundException {
        IwdsLog.i(TAG, "+++++++++++getMd5ByFile");
        int len = 0;
        String md5Sum = null;
        MessageDigest md5 = null;
        byte[] buf = new byte[4096];
        byte[] localmd5Sum = null;
        try {
            FileInputStream in = new FileInputStream(file);
            md5 = MessageDigest.getInstance("MD5");
            // nan add end
            try {
                while (true) {
                    len = in.read(buf);
                    if (len < 0) break;
                    else {
                        md5.update(buf, 0, len);
                    }
                }
                localmd5Sum = md5.digest();
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (NoSuchAlgorithmException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        String result = "";
        if (localmd5Sum != null) {
            for (int j = 0; j < localmd5Sum.length; j++) {
                result += Integer.toString((localmd5Sum[j] & 0xff) + 0x100, 16).substring(1);
            }
        }

        md5Sum = result;
        return md5Sum;
    }

    public static boolean updatezipIsExist() {
        File zipfile = new File(default_updatefilename);
        if (zipfile.exists()) {
            return true;
        }
        return false;
    }

    public static void setUpdatezipDefaultMD5(Context context) {
        File zipfile = new File(default_updatefilename);
        if (zipfile.exists()) {
            try {
                String md5 = getMd5ByFile(zipfile);
                setUpdatezipMD5(context, md5);
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            setUpdatezipMD5(context, DEFAULT_UPDATE_ZIP_MD5);
        }
    }

    /**
     * Returns true if Monkey is running.
     */
    public static boolean isMonkeyRunning() {
        return ActivityManager.isUserAMonkey();
    }

    public static boolean isRadioAllowed(Context context, String type) {
        if (!AirPlaneModeEnabler.isAirplaneModeOn(context)) {
            return true;
        }

        // Here we use the same logic in onCreate().
        String toggleable = Settings.Global.getString(context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_TOGGLEABLE_RADIOS);
        return toggleable != null && toggleable.contains(type);
    }

    /**
     * 判断当前的屏是园还是方形
     * @return
     */
    public static boolean IsCircularScreen(){
        return HardwareList.IsCircularScreen();
    }
}
