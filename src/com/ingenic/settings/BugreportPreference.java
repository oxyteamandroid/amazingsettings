/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings;

import com.ingenic.iwds.app.AmazingDialog;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.SystemProperties;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;

public class BugreportPreference extends DialogPreference {

    private AmazingDialog mDialog;

    private CharSequence mDialogTitle;
    private int mDialogLayoutResId;

    public BugreportPreference(Context context, AttributeSet attrs) {
        this(context, attrs, com.android.internal.R.attr.dialogPreferenceStyle);
    }

    public BugreportPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        TypedArray a = context.obtainStyledAttributes(attrs,
                com.android.internal.R.styleable.DialogPreference, defStyle, 0);
        mDialogTitle = a.getString(com.android.internal.R.styleable.DialogPreference_dialogTitle);
        if (mDialogTitle == null) {
            // Fallback on the regular title of the preference
            // (the one that is seen in the list)
            mDialogTitle = getTitle();
        }

        mDialogLayoutResId = a.getResourceId(
                com.android.internal.R.styleable.DialogPreference_dialogLayout, mDialogLayoutResId);
        a.recycle();
    }

    @Override
    protected void onPrepareDialogBuilder(Builder builder) {
        super.onPrepareDialogBuilder(builder);
        builder.setPositiveButton(com.android.internal.R.string.report, this);
        builder.setMessage(com.android.internal.R.string.bugreport_message);
    }

    @Override
    protected void showDialog(Bundle state) {
        Context context = getContext();

        mDialog = new AmazingDialog(context);
        mDialog.setContent(com.android.internal.R.string.bugreport_message);
        mDialog.setPositiveButton(0, new OnClickListener() {

            @Override
            public void onClick(View v) {
                SystemProperties.set("ctl.start", "bugreport");
                mDialog.dismiss();
            }
        });

        mDialog.setNegativeButton(0, new OnClickListener() {

            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        // getPreferenceManager().registerOnActivityDestroyListener(this);

        // Create the dialog
        if (state != null) {
            mDialog.onRestoreInstanceState(state);
        }

        mDialog.setOnDismissListener(this);
        mDialog.show();
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            SystemProperties.set("ctl.start", "bugreport");
        }
    }

    @Override
    public void onActivityDestroy() {
        if (mDialog == null || !mDialog.isShowing()) {
            return;
        }

        mDialog.dismiss();
    }
}
