/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 675 Mass
 * Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings;

import com.ingenic.iwds.widget.AmazingFocusListView;
import com.ingenic.iwds.widget.CircleImageView;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.IPowerManager;
import android.os.PowerManager;
import android.os.ServiceManager;
import android.provider.Settings;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class BrightnessSettings extends Fragment implements AmazingFocusListView.OnFocusChangedListener {

    private AmazingFocusListView mListView;
    private BrightAdapter mAdapter;
    private int mCurBrightness = -1;
    private int mMaxBrightness;
    private int mMinBrightness;
    private int mBrightnessUnit;
    private boolean mScreenisRound = false;
    private Handler mHandler = new Handler();
    private SetBrightnessRunnable mRunnable = new SetBrightnessRunnable();

    public BrightnessSettings() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView;

        if (mScreenisRound) {
            rootView = inflater.inflate(R.layout.fragment_list_round, container,false);
        } else  {
            rootView = inflater.inflate(R.layout.fragment_list, container,false);
        }

        mListView = (AmazingFocusListView) rootView.findViewById(R.id.list);
        mListView.addHeaderView(createTitleView(R.string.title_bright));
        mListView.addFooterView(new View(getActivity()));
        mListView.setAdapter(mAdapter);
        mListView.setFocusPosition(mAdapter.getCount() - getBrightness());
        mListView.setOnFocusChangedListener(this);
        mListView.setFocusMode(AmazingFocusListView.FOCUS_MODE_CLICK);

        return rootView;
    }

    private View createTitleView(int titleRes) {
        TextView view = new TextView(getActivity());
        view.setGravity(Gravity.CENTER);
        view.setTextAppearance(getActivity(),
                android.R.style.TextAppearance_Large);
        view.setText(titleRes);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        String[] entries = activity.getResources().getStringArray(
                R.array.bright_entries);
        mScreenisRound = Utils.IsCircularScreen();
        if (mScreenisRound) {
            mAdapter = new BrightAdapter(activity, R.layout.list_item_round, entries);
        } else {
            mAdapter = new BrightAdapter(activity, R.layout.list_item, entries);
        }
        PowerManager pm = (PowerManager) activity
                .getSystemService(Context.POWER_SERVICE);
        mMaxBrightness = pm.getMaximumScreenBrightnessSetting();
        mMinBrightness = pm.getMinimumScreenBrightnessSetting();
        mBrightnessUnit = (mMaxBrightness - mMinBrightness) / entries.length;

        if (getMode() == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
            setMode(Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
        }
    }

    private void setMode(int mode) {
        Settings.System.putInt(getActivity().getContentResolver(),
                Settings.System.SCREEN_BRIGHTNESS_MODE, mode);
    }

    private int getMode() {
        return Settings.System.getInt(getActivity().getContentResolver(),
                Settings.System.SCREEN_BRIGHTNESS_MODE,
                Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC);
    }

    private void setBrightness(int value, boolean write) {
        int brightness = value * mBrightnessUnit;

        brightness = Math.max(mMinBrightness,
                Math.min(brightness, mMaxBrightness));
        int index = brightness / mBrightnessUnit;

        if (value != index) {
            mListView.setFocusPosition(index);
        }

        try {
            IPowerManager ipm = IPowerManager.Stub.asInterface(ServiceManager
                    .getService("power"));
            if (ipm != null) {
                ipm.setTemporaryScreenBrightnessSettingOverride(brightness);
            }

            if (write) {
                mCurBrightness = -1;
                Settings.System.putInt(getActivity().getContentResolver(),
                        Settings.System.SCREEN_BRIGHTNESS, brightness);
            } else {
                mCurBrightness = brightness;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getBrightness() {
        int brightness = 0;

        if (mCurBrightness < 0) {
            brightness = Settings.System.getInt(getActivity()
                    .getContentResolver(), Settings.System.SCREEN_BRIGHTNESS,
                    100);
        } else {
            brightness = mCurBrightness;
        }

        brightness = Math.max(mMinBrightness,
                Math.min(brightness, mMaxBrightness));

        return brightness / mBrightnessUnit;
    }

    private static class BrightAdapter extends ArrayAdapter<CharSequence> {

        private Context mContext;
        private int mResource;

        public BrightAdapter(Context context, int resource,
                CharSequence[] objects) {
            super(context, resource, objects);
            mContext = context;
            mResource = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(mResource, parent, false);

                holder = new ViewHolder();
                holder.icon = (CircleImageView) convertView.findViewById(android.R.id.icon);
                holder.text = (TextView) convertView.findViewById(R.id.text);
                holder.icon.setImageResource(R.drawable.ic_brightness);
                holder.icon.setCircleType(CircleImageView.TYPE_STROKE);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.text.setText(getItem(position));
            return convertView;
        }

        private static class ViewHolder {
            CircleImageView icon;
            TextView text;
        }
    }

    @Override
    public void onFocusChanged(AmazingFocusListView view, final int focus, int oldFocus) {
        mHandler.removeCallbacks(mRunnable);
        mRunnable.excute(mAdapter.getCount() - focus);
        mHandler.post(mRunnable);
    }

    private class SetBrightnessRunnable implements Runnable {

        private int mBrightness;

        private void excute(int brightness) {
            mBrightness = brightness;
        }

        @Override
        public void run() {
            setBrightness(mBrightness, true);
        }
    }
}
