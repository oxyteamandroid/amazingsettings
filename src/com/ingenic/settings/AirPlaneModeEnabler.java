/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings;

import com.android.internal.telephony.TelephonyProperties;
import com.ingenic.iwds.utils.IwdsLog;

import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.provider.Settings;

public class AirPlaneModeEnabler extends Enabler {

    private Context mContext;

    private ContentObserver mPlaneModeObserver = new ContentObserver(new Handler()) {

        @Override
        public void onChange(boolean selfChange) {
            onAirPlaneModeChanged();
        }
    };

    public AirPlaneModeEnabler(Context context) {
        mContext = context;
    }

    @Override
    public void setEnable(boolean enabled) {
        if (Boolean.parseBoolean(SystemProperties.get(TelephonyProperties.PROPERTY_INECM_MODE))) {
            // In ECM mode, do not update database at this point
        } else {
            IwdsLog.d(this, "Start set airplane mode");
            setAirPlaneModeOn(enabled);
            setChecked(enabled);
            setClickable(false);
            performStatusChange(enabled);
        }
    }

    @Override
    public boolean isEnabled() {
        return isAirplaneModeOn(mContext);
    }

    public static boolean isAirplaneModeOn(Context context) {
        return Settings.Global.getInt(context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }

    private void setAirPlaneModeOn(boolean enabling) {
        Settings.Global.putInt(mContext.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON,
                enabling ? 1 : 0);

        Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        intent.putExtra("state", enabling);
        mContext.sendBroadcastAsUser(intent, UserHandle.ALL);
    }

    public void setAirplaneModeInECM(boolean isECMExit, boolean isAirplaneModeOn) {
        if (isECMExit) {
            // update database based on the current checkbox state
            setAirPlaneModeOn(isAirplaneModeOn);
        } else {
            // update summary
            onAirPlaneModeChanged();
        }
    }

    private void onAirPlaneModeChanged() {
        IwdsLog.i(this, "End set airplane mode");
        setClickable(true);
    }

    public void resume() {
        mContext.getContentResolver().registerContentObserver(
                Settings.Global.getUriFor(Settings.Global.AIRPLANE_MODE_ON), true,
                mPlaneModeObserver);
    }

    public void pause() {
        mContext.getContentResolver().unregisterContentObserver(mPlaneModeObserver);
    }
}
