/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings;

import android.app.Fragment;
import android.os.Bundle;

import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.widget.RightScrollView;

public class SubSettings extends RightScrollActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean isround =  Utils.IsCircularScreen();
        RightScrollView view = getRightScrollView();
        if (isround ) {
           view.setContentView(R.layout.activity_sub_round);
           view.setBackgroundResource(R.drawable.round);
        } else {
           view.setContentView(R.layout.activity_sub);
        }

        if (savedInstanceState == null) {
            String fragmentClass = getIntent().getStringExtra("fragment");
            if (fragmentClass == null) {
                finish();
            }

            Fragment fragment = Fragment.instantiate(this, fragmentClass);
            getFragmentManager().beginTransaction().add(R.id.container, fragment).commit();
        }
    }
}