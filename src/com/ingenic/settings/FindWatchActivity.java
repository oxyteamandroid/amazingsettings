/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings;

import java.io.IOException;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Vibrator;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.utils.IwdsLog;

public class FindWatchActivity extends RightScrollActivity {

    protected static final String TAG = FindWatchActivity.class.getSimpleName();
    private static final long FIND_MOBILE_TIMEOUT_MIN_2 = 1000 * 10;;
    private ImageView mRotate;
    private AudioManager mAudioManager;
    private static int mStreamVolume;
    private static WakeLock mWakeLock;

    private Runnable mRunnable;
    private Handler mHandler;
    private MediaPlayer mMediaPlayer;
    private Vibrator vibrator;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        IwdsLog.d(TAG, "onNewIntent");
        if (null != mRunnable)
            mHandler.removeCallbacks(mRunnable);
        mRunnable = null;
        startVoice();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_mobile_watch);
        mRotate = (ImageView) findViewById(R.id.rotate);
        mHandler = new Handler();
        findViewById(R.id.click).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                finish();
            }
        });
        setMaxVolumn();
        startVoice();
        startAnim();
        wakeUpAndUnlock(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        restoreVolumn();
        staopVoice();
        closeScreen();
    }

    public void wakeUpAndUnlock(Context context) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        // mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "bright");
        mWakeLock = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.SCREEN_DIM_WAKE_LOCK, "bright");
        // mWakeLock = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP, "bright");
        if (!mWakeLock.isHeld())
            mWakeLock.acquire();
        IwdsLog.d(TAG, "wakeUp");
    }

    public void closeScreen() {
        IwdsLog.d(TAG, "wakeUp release");
        if (mWakeLock.isHeld())
            mWakeLock.release();
    }

    private void staopVoice() {
        if (null != mMediaPlayer && mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
            mMediaPlayer = null;
        }
    }

    private void startVoice() {
        IwdsLog.d(TAG, "startVoice");
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (null != mMediaPlayer && mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
        }
        mMediaPlayer = MediaPlayer.create(this, R.raw.nassau);
        try {
            mMediaPlayer.setLooping(true);
            mMediaPlayer.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mMediaPlayer.start();
        try {
            long[] pattern = { 100, 400, 100, 400 };
            vibrator.vibrate(pattern, -1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mRunnable = new Runnable() {
            @Override
            public void run() {
                if (isFinishing())
                    return;
                IwdsLog.d(TAG, "timeout");
                finish();
            }
        };
        mHandler.postDelayed(mRunnable, FIND_MOBILE_TIMEOUT_MIN_2);

    }

    public void setMaxVolumn() {
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int streamVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        IwdsLog.d(TAG, "getStreamVolume ---> " + streamVolume);

        mStreamVolume = streamVolume;

        int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, 0);
        IwdsLog.d(TAG, "maxVolume ---> " + maxVolume);
    }

    public void restoreVolumn() {
        IwdsLog.d(TAG, "setStreamVolume ---> " + mStreamVolume);
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mStreamVolume, 0);
    }

    private void startAnim() {
        Animation operatingAnim = AnimationUtils.loadAnimation(this, R.anim.anim_find_mobile_activity);
        LinearInterpolator lin = new LinearInterpolator();
        operatingAnim.setInterpolator(lin);
        mRotate.startAnimation(operatingAnim);
    }

    public static void startFindActivity(Context context) {
        Intent intent = new Intent(context, FindWatchActivity.class);
        if (!(context instanceof Activity))
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

}
