/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings;

import com.ingenic.iwds.app.RightScrollActivity;

import android.app.backup.IBackupManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SetFullBackupPassword extends RightScrollActivity {

    static final String TAG = "SetFullBackupPassword";

    IBackupManager mBackupManager;
    TextView mCurrentPw, mNewPw, mConfirmNewPw;
    Button mCancel, mSet;
    private boolean mScreenisRound =false;

    OnClickListener mButtonListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == mSet) {
                final String curPw = mCurrentPw.getText().toString();
                final String newPw = mNewPw.getText().toString();
                final String confirmPw = mConfirmNewPw.getText().toString();

                if (!newPw.equals(confirmPw)) {
                    // Mismatch between new pw and its confirmation re-entry
                    Log.i(TAG, "password mismatch");
                    Toast.makeText(SetFullBackupPassword.this,
                            R.string.local_backup_password_toast_confirmation_mismatch,
                            Toast.LENGTH_LONG).show();
                    return;
                }

                // TODO: should we distinguish cases of has/hasn't set a pwd
                // before?

                if (setBackupPassword(curPw, newPw)) {
                    // success
                    Log.i(TAG, "password set successfully");
                    Toast.makeText(SetFullBackupPassword.this,
                            R.string.local_backup_password_toast_success, Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    // failure -- bad existing pw, usually
                    Log.i(TAG, "failure; password mismatch?");
                    Toast.makeText(SetFullBackupPassword.this,
                            R.string.local_backup_password_toast_validation_failure,
                            Toast.LENGTH_LONG).show();
                }
            } else if (v == mCancel) {
                finish();
            } else {
                Log.w(TAG, "Click on unknown view");
            }
        }
    };

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        mBackupManager = IBackupManager.Stub.asInterface(ServiceManager.getService("backup"));
        mScreenisRound = Utils.IsCircularScreen();
        if (mScreenisRound) {
            setContentView(R.layout.set_backup_pw_round);
        } else {
            setContentView(R.layout.set_backup_pw);
        }

        mCurrentPw = (TextView) findViewById(R.id.current_backup_pw);
        mNewPw = (TextView) findViewById(R.id.new_backup_pw);
        mConfirmNewPw = (TextView) findViewById(R.id.confirm_new_backup_pw);

        mCancel = (Button) findViewById(R.id.backup_pw_cancel_button);
        mSet = (Button) findViewById(R.id.backup_pw_set_button);

        mCancel.setOnClickListener(mButtonListener);
        mSet.setOnClickListener(mButtonListener);
    }

    private boolean setBackupPassword(String currentPw, String newPw) {
        try {
            return mBackupManager.setBackupPassword(currentPw, newPw);
        } catch (RemoteException e) {
            Log.e(TAG, "Unable to communicate with backup manager");
            return false;
        }
    }
}