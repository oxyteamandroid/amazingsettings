/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.XmlResourceParser;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IPowerManager;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.util.Log;

import com.ingenic.iwds.app.AmazingDialog;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.widget.AdapterView;
import com.ingenic.iwds.widget.AmazingFocusListView;
import com.ingenic.iwds.widget.CircleImageView;
import com.ingenic.iwds.widget.RightScrollView;
import com.ingenic.settings.bluetooth.BluetoothDiscoverableEnabler;
import com.ingenic.settings.wifi.WifiEnabler;

import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.smartsense.Sensor;
import com.ingenic.iwds.smartsense.SensorEvent;
import com.ingenic.iwds.smartsense.SensorEventListener;
import com.ingenic.iwds.smartsense.SensorServiceManager;

public class Settings extends RightScrollActivity implements
        AdapterView.OnItemClickListener {

    private AmazingFocusListView mListView;
    private HeaderAdapter mAdapter;
    private List<Header> mHeaders;
    private AmazingDialog mAmazingDialog;

    public Context mmcontext;
    public ServiceClient mClient;
    public SensorServiceManager mService;
    public Sensor mmgesture;
    public Sensor mmvoicetriggle;
    public fake mfake;
    public boolean gesture_isexist = false;
    public boolean voicetriggle_isexist = false;

    private SharedPreferences mDevelopmentPreferences;
    private SharedPreferences.OnSharedPreferenceChangeListener mDevelopmentPreferencesListener;
    private static boolean mScreenisRound = false;
    // 右滑控件
    private RightScrollView mView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDevelopmentPreferences = getSharedPreferences(DevelopmentSettings.PREF_FILE,
                Context.MODE_PRIVATE);
        mView = getRightScrollView();
        mView.disableRightScroll();
        mScreenisRound = Utils.IsCircularScreen();
        if (mScreenisRound){
            mView.setContentView(R.layout.activity_settings_round);
            mView.setBackgroundResource(R.drawable.round);
        } else {
            mView.setContentView(R.layout.activity_settings);
        }
        mDevelopmentPreferences = getSharedPreferences(
                DevelopmentSettings.PREF_FILE, Context.MODE_PRIVATE);

        setContentView(R.layout.activity_settings);

        mfake = new fake();
        mmcontext = getBaseContext();
        mClient = new ServiceClient(mmcontext,
                ServiceManagerContext.SERVICE_SENSOR, mfake);
        mClient.connect();

        mHeaders = parserHeaders();
        mAdapter = new HeaderAdapter(this, new ArrayList<Header>());

        mListView = (AmazingFocusListView) findViewById(R.id.list);
        mListView.addHeaderView(createTitleView(R.string.app_name));
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
        mListView.setFocusPosition(0);
    }

    private View createTitleView(int titleRes) {
        TextView view = new TextView(this);
        view.setGravity(Gravity.CENTER);
        view.setTextAppearance(this, android.R.style.TextAppearance_Large);
        view.setText(titleRes);
        return view;
    }

    private class fake implements ServiceClient.ConnectionCallbacks {
        @Override
        public void onConnected(ServiceClient serviceClient) {
            mService = (SensorServiceManager) mClient
                    .getServiceManagerContext();
            mmgesture = mService.getDefaultSensor(Sensor.TYPE_GESTURE);
            mmvoicetriggle = mService
                    .getDefaultSensor(Sensor.TYPE_VOICE_TRIGGER);
            if (mmgesture == null) {
                gesture_isexist = false;
            } else {
                gesture_isexist = true;
            }
            if (mmvoicetriggle == null) {
                voicetriggle_isexist = false;
            } else {
                voicetriggle_isexist = true;
            }
            mHeaders = parserHeaders();
            updateHeaders();
        }

        @Override
        public void onDisconnected(ServiceClient serviceClient,
                boolean unexpected) {

        }

        @Override
        public void onConnectFailed(ServiceClient serviceClient,
                ConnectFailedReason reason) {

        }

    };

    @Override
    protected void onResume() {
        super.onResume();

        mDevelopmentPreferencesListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(
                    SharedPreferences sharedPreferences, String key) {
                updateHeaders();
            }
        };

        mDevelopmentPreferences
                .registerOnSharedPreferenceChangeListener(mDevelopmentPreferencesListener);

        updateHeaders();
        mAdapter.resume();
        mView.enableRightScroll();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mAdapter.pause();

        mDevelopmentPreferences
                .unregisterOnSharedPreferenceChangeListener(mDevelopmentPreferencesListener);
        mDevelopmentPreferencesListener = null;
    }

    public List<Header> getHeaders() {
        return mHeaders;
    }

    private void updateHeaders() {
        List<Header> headers = getHeaders();
        final boolean showDev = mDevelopmentPreferences.getBoolean(
                DevelopmentSettings.PREF_SHOW,
                android.os.Build.TYPE.equals("eng"));

        mAdapter.clear();
        for (Header header : headers) {
            boolean add = false;
            if (header.icon != 0 || header.title != null
                    || header.fragment != null) {
                add = true;
            }

            if (header.id == R.id.development) {
                add = showDev;
            }

            if (add) {
                mAdapter.add(header);
            }
        }

        mAdapter.notifyDataSetChanged();
    }

    private static class HeaderAdapter extends ArrayAdapter<Header> implements
            Enabler.OnStatusChangedListener {

        private Context mContext;
        private final BluetoothDiscoverableEnabler mBluetoothEnabler;
        private final AirPlaneModeEnabler mPlaneModeEnabler;
        private final WifiEnabler mWifiEnabler;
        private final GestureEnabler mGestureEnabler;
        private final VoicetriggleEnabler mVoicetriggleEnabler;

        public HeaderAdapter(Context context, List<Header> objects) {
            super(context, -1, objects);

            mContext = context;
            mBluetoothEnabler = BluetoothDiscoverableEnabler
                    .getInstance(mContext);
            mPlaneModeEnabler = new AirPlaneModeEnabler(context);
            mWifiEnabler = new WifiEnabler(context);
            mGestureEnabler = new GestureEnabler(context);
            mVoicetriggleEnabler = new VoicetriggleEnabler(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
                if (mScreenisRound) {
                    convertView = inflater.inflate(R.layout.header_normal_round, parent, false);
                } else {
                    convertView = inflater.inflate(R.layout.header_normal, parent, false);
                }

                holder = new ViewHolder();
                holder.icon = (CircleImageView) convertView
                        .findViewById(android.R.id.icon);
                holder.title = (TextView) convertView.findViewById(R.id.title);
                holder.summary = (TextView) convertView
                        .findViewById(R.id.summary);
                holder.icon.setCircleType(CircleImageView.TYPE_STROKE);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Header header = getItem(position);
            holder.icon.setImageResource(header.icon);
            holder.title.setText(header.title);

            Enabler enabler = getEnabler(header.id);
            switch (header.id) {
            case R.id.volumn:
                header.summary = getVolumnSummary();
                break;
            case R.id.brightness:
                header.summary = getBrightSummary();
                break;
            default:
                break;
            }

            if (enabler != null) {
                header.summary = enabler.getSummary(mContext);
                enabler.setOnStatusChangedListener(this);
                enabler.setup(holder.icon);
            } else {
                holder.icon.setChecked(false);
            }

            if (header.summary == null) {
                holder.summary.setVisibility(View.GONE);
            } else {
                holder.summary.setVisibility(View.VISIBLE);
                holder.summary.setText(header.summary);
            }

            return convertView;
        }

        private String getVolumnSummary() {
            AudioManager am = (AudioManager) mContext
                    .getSystemService(AUDIO_SERVICE);
            int volumn = am.getStreamVolume(AudioManager.STREAM_RING);
            String[] volumns = mContext.getResources().getStringArray(
                    R.array.volumn_entries);
            return volumns[volumns.length - volumn - 1];
        }

        private String getBrightSummary() {
            int brightness = android.provider.Settings.System.getInt(
                    mContext.getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS, 100);
            PowerManager pm = (PowerManager) mContext
                    .getSystemService(POWER_SERVICE);
            int max = pm.getMaximumScreenBrightnessSetting();
            int min = pm.getMinimumScreenBrightnessSetting();

            String[] brights = mContext.getResources().getStringArray(
                    R.array.bright_entries);
            int index = brightness * brights.length / (max - min);
            return String.valueOf(index);
        }

        @Override
        public void onStatusChanged(Enabler enabler, boolean enabled) {
            notifyDataSetChanged();
        }

        private void resume() {
            mBluetoothEnabler.resume();
            mWifiEnabler.resume();
            mPlaneModeEnabler.resume();
        }

        private void pause() {
            mBluetoothEnabler.pause();
            mWifiEnabler.pause();
            mPlaneModeEnabler.pause();
        }

        Enabler getEnabler(int id) {
            switch (id) {
            case R.id.bluetooth_discovery:
                return mBluetoothEnabler;
            case R.id.airplane_mode:
                return mPlaneModeEnabler;
            case R.id.gesture:
                return mGestureEnabler;
            case R.id.voicetriggle:
                return mVoicetriggleEnabler;
            default:
                return null;
            }
        }

        @Override
        public boolean isEnabled(int position) {
            Header header = getItem(position);
            int id = header.id;

            Enabler enabler = getEnabler(id);
            if (enabler != null) {
                return enabler.isClickable();
            }

            return super.isEnabled(position);
        }

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        private static class ViewHolder {
            CircleImageView icon;
            TextView title;
            TextView summary;
        }
    }

    private static class Header {
        int id;
        int icon;
        CharSequence title;
        CharSequence summary;
        String fragment;
    }

    private List<Header> parserHeaders() {
        XmlResourceParser parser = getResources()
                .getXml(R.xml.settings_headers);
        List<Header> headers = new ArrayList<Header>();

        try {
            int eventType = parser.getEventType();

            while (eventType != XmlResourceParser.END_DOCUMENT) {
                switch (eventType) {
                case XmlResourceParser.START_TAG:
                    if ("header".equalsIgnoreCase(parser.getName())) {
                        Header header = parserHeader(parser);
                        if (header != null && hardware_exit(header.id)) {
                            headers.add(header);
                        }
                    }
                    break;
                }
                eventType = parser.next();
            }
        } catch (Exception e) {
        }
        return headers;
    }

    boolean hardware_exit(int id) {
        switch (id) {
        case R.id.gesture:
            return gesture_isexist;
        case R.id.voicetriggle:
            return voicetriggle_isexist;
        default:
            return true;
        }
    }

    private Header parserHeader(XmlResourceParser parser) {
        Header header = new Header();

        int count = parser.getAttributeCount();
        for (int i = 0; i < count; i++) {
            String attr = parser.getAttributeName(i);
            int id = parser.getAttributeResourceValue(i, 0);

            if ("id".equals(attr)) {
                header.id = id;
            } else if ("icon".equals(attr)) {
                header.icon = id;
            } else if ("title".equals(attr)) {
                if (id == 0) {
                    header.title = parser.getAttributeValue(i);
                } else {
                    header.title = getString(id);
                }
            } else if ("summary".equals(attr)) {
                if (id == 0) {
                    header.summary = parser.getAttributeValue(i);
                } else {
                    header.summary = getString(id);
                }
            } else if ("fragment".equals(attr)) {
                header.fragment = parser.getAttributeValue(i);
            }
        }

        return header;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
            long id) {
        onHeaderClick(position);
    }

    private void onHeaderClick(int position) {
        Header header = mAdapter.getItem(position);

        if (header.fragment != null) {
            startFragment(header.fragment);
            getRightScrollView().disableRightScroll();
        } else {
            Enabler enabler = mAdapter.getEnabler(header.id);
            if (enabler != null) {
                enabler.setEnable(!enabler.isEnabled());
            } else {
                final IPowerManager ipm = IPowerManager.Stub
                        .asInterface(ServiceManager.getService("power"));
                switch (header.id) {
                case R.id.reboot:
                    if (ipm != null) {
                        showRebootDialog(ipm, true);
                    }
                    break;
                case R.id.shutdown:
                    if (ipm != null) {
                        showRebootDialog(ipm, false);
                    }
                    break;
                }
            }
        }
    }

    private void showRebootDialog(final IPowerManager ipm, final boolean reboot) {
        getRightScrollView().disableRightScroll();

        if (mAmazingDialog == null) {
            mAmazingDialog = new AmazingDialog(Settings.this);
        }

        if (mAmazingDialog.isShowing())
            return;

        if (reboot) {// reboot
            mAmazingDialog.setContent(R.string.content_reboot);
        } else {// shutdown
            mAmazingDialog.setContent(R.string.content_turn_off);
        }

        mAmazingDialog.setPositiveButton(0, new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                try {
                    if (reboot)
                        ipm.reboot(false, null, false);
                    else
                        ipm.shutdown(false, false);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                mAmazingDialog.dismiss();
            }
        }).setNegativeButton(0, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAmazingDialog.dismiss();
            }
        });

        mAmazingDialog.setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss(DialogInterface dialog) {
                getRightScrollView().enableRightScroll();
            }
        });

        mAmazingDialog.show();
    }

    private void startFragment(String fragmentClass) {
        Intent it = new Intent(this, SubSettings.class);
        it.putExtra("fragment", fragmentClass);
        startActivity(it);
    }
}
