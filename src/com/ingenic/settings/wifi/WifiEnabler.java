/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings.wifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.widget.Toast;

import com.ingenic.settings.Enabler;
import com.ingenic.settings.R;
import com.ingenic.settings.Utils;

public class WifiEnabler extends Enabler {

    private Context mContext;
    private WifiManager mWifiManager;
    private boolean mStateMachineEvent;
    private final IntentFilter mIntentFilter;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(action)) {
                handleWifiStateChanged(intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE,
                        WifiManager.WIFI_STATE_UNKNOWN));
            }
        }
    };

    public WifiEnabler(Context context) {
        mContext = context;
        mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        mIntentFilter = new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION);
    }

    public void resume() {
        // Wi-Fi state is sticky, so just let the receiver update UI
        mContext.registerReceiver(mReceiver, mIntentFilter);
    }

    public void pause() {
        mContext.unregisterReceiver(mReceiver);
    }

    @Override
    public void setEnable(boolean enabled) {
        if (mStateMachineEvent) {
            return;
        }

        // Show toast message if Wi-Fi is not allowed in airplane mode
        if (enabled && !Utils.isRadioAllowed(mContext, Settings.Global.RADIO_WIFI)) {
            Toast.makeText(mContext, R.string.wifi_in_airplane_mode, Toast.LENGTH_SHORT).show();
            // Reset switch to off. No infinite check/listenenr loop.
            //            mCheckable.setChecked(false);
            return;
        }

        // Disable tethering if enabling Wifi
        int wifiApState = mWifiManager.getWifiApState();
        if (enabled
                && ((wifiApState == WifiManager.WIFI_AP_STATE_ENABLING) || (wifiApState == WifiManager.WIFI_AP_STATE_ENABLED))) {
            mWifiManager.setWifiApEnabled(null, false);
        }

        if (mWifiManager.setWifiEnabled(enabled)) {
            // Intent has been taken into account, disable until new state is
            // active
        } else {
            Toast.makeText(mContext, R.string.wifi_error, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean isEnabled() {
        final int wifiState = mWifiManager.getWifiState();
        return wifiState == WifiManager.WIFI_STATE_ENABLED;
    }

    private void handleWifiStateChanged(int state) {
        if (mCheckable == null) return;
        switch (state) {
        case WifiManager.WIFI_STATE_ENABLING:
            setClickable(false);
            setChecked(true);
            break;
        case WifiManager.WIFI_STATE_ENABLED:
            setButtonChecked(true);
            setClickable(true);
            setChecked(true);
            break;
        case WifiManager.WIFI_STATE_DISABLING:
            setClickable(false);
            setChecked(false);
            break;
        case WifiManager.WIFI_STATE_DISABLED:
            setButtonChecked(false);
            setClickable(true);
            setChecked(false);
            break;
        default:
            setButtonChecked(false);
            setClickable(true);
            break;
        }

        performStatusChange(isEnabled());
    }

    private void setButtonChecked(boolean checked) {
        if (checked != mCheckable.isChecked()) {
            mStateMachineEvent = true;
            setEnable(checked);
            mStateMachineEvent = false;
        }
    }
}