/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * WuZhiming(Mike)<zhiming.wu@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 675 Mass
 * Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings;

import com.ingenic.iwds.widget.AmazingFocusListView;
import com.ingenic.iwds.widget.CircleImageView;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.provider.Settings;

public class GestureSettings extends Fragment implements
        AmazingFocusListView.OnFocusChangedListener {
    // the GestureService will listen the preference of the TAG. should not
    // change it.
    private final static String TAG = "Gesture";
    private final static String GESTURE_STATE = "gesture_on";

    private AmazingFocusListView mListView;
    private GestureAdapter mAdapter;
    private Handler mHandler = new Handler();
    private SetVolumnRunnable mRunnable = new SetVolumnRunnable();

    private Context mContext;
    private boolean mScreenisRound = false;

    private static final int[] STREAM_TYPE = new int[] {
            AudioManager.STREAM_MUSIC, AudioManager.STREAM_RING,
            AudioManager.STREAM_NOTIFICATION, AudioManager.STREAM_ALARM };

    public GestureSettings() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        String[] entries = activity.getResources().getStringArray(
                R.array.gesture_entries);
        mScreenisRound = Utils.IsCircularScreen();
        if (mScreenisRound) {
            mAdapter = new GestureAdapter(activity, R.layout.list_item_round, entries, 3);
        } else {
            mAdapter = new GestureAdapter(activity, R.layout.list_item, entries, 3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView;
        if (mScreenisRound) {
            rootView = inflater.inflate(R.layout.fragment_list_round, container,false);
        } else  {
            rootView = inflater.inflate(R.layout.fragment_list, container,false);
        }

        mListView = (AmazingFocusListView) rootView.findViewById(R.id.list);
        mListView.addHeaderView(createTitleView(R.string.title_gesture));
        mListView.addFooterView(new View(getActivity()));
        mListView.setAdapter(mAdapter);
        int pos = getIntPref(mContext, GESTURE_STATE, 0);
        mListView.setFocusPosition(mAdapter.getCount() - pos - 1);
        mListView.setOnFocusChangedListener(this);
        mListView.setFocusMode(AmazingFocusListView.FOCUS_MODE_CLICK);
        return rootView;
    }

    private View createTitleView(int titleRes) {
        TextView view = new TextView(getActivity());
        view.setGravity(Gravity.CENTER);
        view.setTextAppearance(getActivity(),
                android.R.style.TextAppearance_Large);
        view.setText(titleRes);
        return view;
    }

    private static class GestureAdapter extends ArrayAdapter<CharSequence> {
        private Context mContext;
        private int mResource;
        private int mMax;

        @Override
        public int getCount() {
            return mMax;
        }

        @Override
        public CharSequence getItem(int position) {
            switch (position) {
            case 2:
                return mContext.getString(R.string.summary_close);
            case 1:
                return mContext.getString(R.string.gesture_right);
            case 0:
                return mContext.getString(R.string.gesture_left);
            default:
                return mContext.getString(R.string.summary_close);
            }
        }

        public GestureAdapter(Context context, int resource,
                CharSequence[] objects, int maxState) {
            super(context, resource, objects);
            mContext = context;
            mResource = resource;
            mMax = maxState;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(mResource, parent, false);

                holder = new ViewHolder();
                holder.icon = (CircleImageView) convertView
                        .findViewById(android.R.id.icon);
                holder.icon.setImageResource(R.drawable.ic_gesture);
                holder.icon.setCircleType(CircleImageView.TYPE_STROKE);
                holder.text = (TextView) convertView.findViewById(R.id.text);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.text.setText(getItem(position));

            return convertView;
        }

        private static class ViewHolder {
            CircleImageView icon;
            TextView text;
        }
    }

    void setGesture(int state) {
        setIntPref(mContext, GESTURE_STATE, state);
    }

    @Override
    public void onFocusChanged(AmazingFocusListView view, final int focus,
            int oldFocus) {
        mHandler.removeCallbacks(mRunnable);
        int mfocus = mAdapter.getCount() - focus - 1;
        mRunnable.excute(mfocus);
        mHandler.post(mRunnable);
    }

    private class SetVolumnRunnable implements Runnable {

        private int mGesture_state;

        private void excute(int state) {
            mGesture_state = state;
        }

        @Override
        public void run() {
            setGesture(mGesture_state);
        }
    }

    private static int getIntPref(Context context, String name, int def) {
        SharedPreferences prefs = context.getSharedPreferences(TAG,
                Context.MODE_PRIVATE);
        int ret = prefs.getInt(name, def);
        return ret;
    }

    private static void setIntPref(Context context, String name, int value) {
        if(name.equals(GESTURE_STATE))
            Settings.System.putInt(context.getContentResolver(), GESTURE_STATE, value);
        SharedPreferences prefs = context.getSharedPreferences(TAG,
                Context.MODE_PRIVATE);
        Editor ed = prefs.edit();
        ed.putInt(name, value);
        ed.commit();
    }
}
