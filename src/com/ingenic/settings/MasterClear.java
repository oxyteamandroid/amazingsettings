/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 675 Mass
 * Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings;

import com.android.internal.os.storage.ExternalStorageFormatter;
import com.ingenic.iwds.app.AmazingDialog;
import com.ingenic.settings.MasterClear;
import com.ingenic.settings.R;
import com.ingenic.settings.Utils;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemProperties;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class MasterClear extends Fragment {

    private static final int KEYGUARD_REQUEST = 55;
    static final String ERASE_EXTERNAL_EXTRA = "erase_sd";

    private View mContentView;
    private Button mInitiateButton;
    private boolean mEraseSdCard;
    private boolean mScreenisRound = false;

    /**
     * If the user clicks to begin the reset sequence, we next require a
     * keyguard confirmation if the user has currently enabled one. If there is
     * no keyguard available, we simply go to the final confirmation prompt.
     */
    private final View.OnClickListener mInitiateListener = new View.OnClickListener() {

        public void onClick(View v) {
            if (!runKeyguardConfirmation(KEYGUARD_REQUEST)) {
                showFinalConfirmation();
            }
        }
    };

    /**
     * Keyguard validation is run using the standard {@link ConfirmLockPattern}
     * component as a subactivity
     * 
     * @param request
     *            the request code to be returned once confirmation finishes
     * @return true if confirmation launched
     */
    private boolean runKeyguardConfirmation(int request) {
        // Resources res = getActivity().getResources();
        return false;/*
                      * new ChooseLockSettingsHelper(getActivity(), this)
                      * .launchConfirmationActivity(request,
                      * res.getText(R.string.master_clear_gesture_prompt),
                      * res.getText(R.string.master_clear_gesture_explanation));
                      */
    }

    private void showFinalConfirmation() {
        // Intent it = new Intent(getActivity(), SubSettings.class);
        // it.putExtra("fragment", MasterClearConfirm.class.getName());
        // startActivity(it);
        // Preference preference = new Preference(getActivity());
        // preference.setFragment(MasterClearConfirm.class.getName());
        // preference.setTitle(R.string.master_clear_confirm_title);
        // ((PreferenceActivity) getActivity()).onPreferenceStartFragment(null,
        // preference);
        final AmazingDialog dialog = new AmazingDialog(getActivity());
        dialog.setContent(R.string.master_clear_final_desc);
        dialog.setPositiveButton(0, new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (Utils.isMonkeyRunning()) {
                    return;
                }

                if (mEraseSdCard) {
                    Intent intent = new Intent(
                            ExternalStorageFormatter.FORMAT_AND_FACTORY_RESET);
                    intent.setComponent(ExternalStorageFormatter.COMPONENT_NAME);
                    getActivity().startService(intent);
                } else {
                    getActivity().sendBroadcast(
                            new Intent("android.intent.action.MASTER_CLEAR"));
                    // Intent handling is asynchronous -- assume it will happen
                    // soon.
                }
                dialog.dismiss();
            }
        }).setNegativeButton(0, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public MasterClear() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mContentView = inflater
                .inflate(R.layout.master_clear, container, false);
        mScreenisRound = Utils.IsCircularScreen();
        if (mScreenisRound){
            mContentView = inflater
                    .inflate(R.layout.master_clear_round, container, false);
        } else {
            mContentView = inflater
                    .inflate(R.layout.master_clear, container, false);
        }
        Bundle args = getArguments();
        mEraseSdCard = args != null ? args
                .getBoolean(MasterClear.ERASE_EXTERNAL_EXTRA) : false;

        establishInitialState();

        return mContentView;
    }

    /**
     * In its initial state, the activity presents a button for the user to
     * click in order to initiate a confirmation sequence. This method is called
     * from various other points in the code to reset the activity to this base
     * state.
     * 
     * <p>
     * Reinflating views from resources is expensive and prevents us from
     * caching widget pointers, so we use a single-inflate pattern: we lazy-
     * inflate each view, caching all of the widget pointers we'll need at the
     * time, then simply reuse the inflated views directly whenever we need to
     * change contents.
     */
    private void establishInitialState() {
        mInitiateButton = (Button) mContentView
                .findViewById(R.id.initiate_master_clear);
        mInitiateButton.setOnClickListener(mInitiateListener);

        /*
         * If the external storage is emulated, it will be erased with a factory
         * reset at any rate. There is no need to have a separate option until
         * we have a factory reset that only erases some directories and not
         * others. Likewise, if it's non-removable storage, it could potentially
         * have been encrypted, and will also need to be wiped.
         */
        boolean isExtStorageEmulated = Environment.isExternalStorageEmulated();
        if (isExtStorageEmulated
                || (!Environment.isExternalStorageRemovable() && isExtStorageEncrypted())) {

            final View externalAlsoErased = mContentView
                    .findViewById(R.id.also_erases_external);
            externalAlsoErased.setVisibility(View.VISIBLE);

            // If it's not emulated, it is on a separate partition but it means
            // we're doing
            // a force wipe due to encryption.
        }
    }

    private boolean isExtStorageEncrypted() {
        String state = SystemProperties.get("vold.decrypt");
        return !"".equals(state);
    }
}
