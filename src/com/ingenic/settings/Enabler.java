/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings;

import android.content.Context;
import android.view.View;
import android.widget.Checkable;

public abstract class Enabler {
    protected Checkable mCheckable;
    private OnStatusChangedListener mListener;
    private boolean mClickable = true;

    public void setup(Checkable checkable) {
        if (mCheckable != checkable) {
            mCheckable = checkable;
        }

        setChecked(isEnabled());
        setClickable(isClickable());
    }

    protected void setChecked(boolean checked) {
        if (mCheckable == null) return;
        if (checked != mCheckable.isChecked()) {
            mCheckable.setChecked(checked);
        }
    }

    public void setClickable(boolean clickable) {
        mClickable = clickable;
        if (mCheckable instanceof View) {
            ((View) mCheckable).setEnabled(clickable);
        }
    }

    public boolean isClickable() {
        return mClickable;
    }

    public abstract void setEnable(boolean enabled);

    public abstract boolean isEnabled();

    public void setOnStatusChangedListener(OnStatusChangedListener l) {
        if (mListener == l) return;
        mListener = l;
    }

    protected void performStatusChange(boolean enabled) {
        if (mListener != null) {
            mListener.onStatusChanged(this, enabled);
        }
    }

    public interface OnStatusChangedListener {
        void onStatusChanged(Enabler enabler, boolean enabled);
    }

    public String getSummary(Context context) {
        return context.getString(isEnabled() ? R.string.summary_open : R.string.summary_close);
    }
}
