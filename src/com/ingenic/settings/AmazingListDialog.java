/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 * 
 * Elf/iwds-ui-jar Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.ingenic.iwds.widget.RightScrollView;

/**
 * 自定义带文字和按钮的对话框
 */
public class AmazingListDialog extends Dialog implements OnItemClickListener {

    /**
     * 右滑View
     */
    private RightScrollView mRightScrollView;

    /**
     * 对话框布局View
     */
    private View contentView;

    /**
     * 对话框标题
     */
    private TextView title;
    private ListView listView;
    private ListAdapter listAdapter;
    private ItemClickListner mItemClickListner;

    public AmazingListDialog(Context context, CharSequence[] mItems, final int index) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);

        mRightScrollView = new RightScrollView(context);
        mRightScrollView.setContentView(R.layout.list_preference_dialog);
        mRightScrollView.setOnRightScrollListener(new RightScrollView.OnRightScrollListener() {

            @Override
            public void onRightScroll() {
                dismiss();
            }
        });

        listAdapter = new ArrayAdapter<CharSequence>(context, R.layout.select_dialog_singlechoice,
                R.id.text1, mItems);

        title = (TextView) mRightScrollView.findViewById(R.id.dialog_title);
        listView = (ListView) mRightScrollView.findViewById(R.id.dialog_list);
        listView.setAdapter(listAdapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setOnItemClickListener(this);
        listView.setItemChecked(index, true);
        listView.smoothScrollToPosition(index);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(mRightScrollView);
        this.getWindow().setWindowAnimations(R.style.AmazingDialogAnimationFadeNormal);
    }

    /**
     * 获取标题文字
     * 
     * @return 标题TEXT
     */
    public CharSequence setTitleText() {
        return title.getText();
    }

    /**
     * 设置标题文字
     * 
     * @param resId 标题文字的资源ID
     * @return 对话框对象
     */
    public AmazingListDialog setTitleText(int resId) {
        this.title.setText(resId);
        return this;
    }

    /**
     * 设置标题文字
     * 
     * @param text CharSequence类型的标题文字
     * @return 对话框对象
     */
    public AmazingListDialog setTitleText(CharSequence text) {
        this.title.setText(text);
        return this;
    }

    /**
     * 设置标题文字颜色
     * 
     * @param color 文字颜色值
     * @return 对话框对象
     */
    public AmazingListDialog setTitleTextColor(int color) {
        if (title != null) {
            title.setTextColor(color);
        }
        return this;
    }

    /**
     * 设置标题文字大小
     * 
     * @param size 文字大小
     * @return 对话框对象
     */
    public AmazingListDialog setTitleTextSize(int size) {
        if (title != null) {
            title.setTextSize(size);
        }
        return this;
    }

    /**
     * 设置对话框的背景颜色
     * 
     * @param color 背景颜色值
     * @return 对话框对象
     */
    public AmazingListDialog setBackgroundColor(int color) {
        if (contentView != null) {
            contentView.setBackgroundColor(color);
        }
        return this;
    }

    /**
     * 设置对话框的背景图片
     * 
     * @param background 背景图片的Drawable对象
     * @return 对话框对象
     */
    public AmazingListDialog setBackgroundDrawable(Drawable background) {
        if (contentView != null) {
            contentView.setBackground(background);
        }
        return this;
    }

    /**
     * 显示对话框
     */
    @Override
    public void show() {
        mRightScrollView.setAlpha(1.0f);
        mRightScrollView.setTranslationX(0);

        super.show();
    }

    public void setItemClickListner(ItemClickListner listener) {
        if (listener != null) this.mItemClickListner = listener;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mItemClickListner != null) mItemClickListner.onItemClick(position);
    }

    public interface ItemClickListner {
        void onItemClick(int position);
    }
}