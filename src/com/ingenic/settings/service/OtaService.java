/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * nongjiabao<jiabao.nong@ingenic.com>
 *
 * Elf/IDWS Project
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.settings.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RecoverySystem;
import android.view.WindowManager;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.FileInfo;
import com.ingenic.iwds.datatransactor.FileTransactionModel;
import com.ingenic.iwds.datatransactor.FileTransactionModel.FileTransactionModelCallback;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.settings.R;
import com.ingenic.settings.Utils;

public class OtaService extends Service {
    private String TAG = "OtaService";
    private static final int MSG_REPONE_CMD = 1;
    private static final int MSG_REQUESTSENDFILE = 2;
    private static final int MSG_CHANNEL_UNAVAILABLE = 3;
    private static final int MSG_CHANNEL_AVAILABLE = 31;
    private static final int MSG_FILEARRIVD = 4;
    private static final int MSG_DISCONNECT = 5;
    private static final int MSG_SET_PROGRESS = 6;
    /**
     * UUID_OTAFILE 传输升级包的UUID
     */
    private static final String UUID_OTAFILE = "439536e2-cc6a-11e4-90da-5404a6abe086";
    @SuppressLint("SdCardPath")
    private static final String mSdcard= "/mnt/sdcard/";
    private static final String mOtapath = "data/media/0/iwds/update.zip";
    private static long mLength = 0;

    private ProgressDialog mProgress = null;
    private AlertDialog    mConfirm_dlg = null;
    private static MyFiletransModel mFiletransModel;

    private void showProgressDialog() {
        mProgress = new ProgressDialog(this);
        mProgress.setMax(100);
        mProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgress.setTitle(R.string.receive);
        mProgress.setCancelable(false);
        mProgress.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        mProgress.show();
        mProgress.incrementProgressBy(-mProgress.getProgress());
    }

    private void dimissProgressDialog() {
        if (mProgress != null)
            mProgress.dismiss();
    }

    public void showConfirmDialog(final File otafile) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.tips)
                .setMessage(R.string.install_tips)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        installUpdatePkg(otafile);
                    }
                }).setNegativeButton(R.string.no, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        mConfirm_dlg = builder.create();
        mConfirm_dlg.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        mConfirm_dlg.setCancelable(false);
        mConfirm_dlg.show();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate() {
        super.onCreate();
        /**
         * 保持服务一直在运行
         */
        CharSequence label = getApplication().getApplicationInfo().loadLabel(
                getPackageManager());

        Notification notification = new Notification(getApplication()
                .getApplicationInfo().icon, label, System.currentTimeMillis());

        Intent it = new Intent(
                ServiceManagerContext.ACTION_NOTIFICATION_CLICKED);
        it.setFlags(it.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, it, 0);
        notification.setLatestEventInfo(this, label, "", pendingIntent);
        notification.flags = notification.flags
                | Notification.FLAG_ONGOING_EVENT;

        startForeground(9991, notification);

        Context context = getBaseContext();
        Utils.setUpdatezipDefaultMD5(context);
        mFiletransModel = new MyFiletransModel(context, new OtaFileTransfer(), UUID_OTAFILE);
        mFiletransModel.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFiletransModel.stop();
        mFiletransModel = null;
        mProgress = null;
        mConfirm_dlg = null;
        sendBroadcast(new Intent(Utils.ACTION_RESTART_SERVICE));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    private void installUpdatePkg(File file) {

        try {
            File temp = new File(mSdcard);
            if (null != file && file.exists()) {
                if (null != temp && temp.exists()) {
                    try {
                        File files[] = temp.listFiles();
                        IwdsLog.d(TAG, "files.len=" + files.length);
                        for (int i = 0; i < files.length; i++) {
                            if (files[i].getName().endsWith(".REC")) {
                                files[i].delete();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                IwdsLog.d(TAG, "installUpdatePkg," + file.getCanonicalPath());
                RecoverySystem.installPackage(this, file);
            }
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    private class MyFiletransModel extends FileTransactionModel {

        public MyFiletransModel(Context context,
                FileTransactionModelCallback callback, String uuid) {
            super(context, callback, uuid);

        }

        public void sendCmd(String cmd) {
            m_transactor.send(cmd);
        }

        @Override
        public void onDataArrived(Object object) {
            super.onDataArrived(object);
            if (object instanceof String) {
                IwdsLog.i(TAG, "onDataArrived command: " + object.toString());
                if (object.equals("updates")) {
                    Message msg = mHandler.obtainMessage(MSG_REPONE_CMD);
                    msg.obj = "recovery";
                    msg.sendToTarget();
                }
            }
        }
    }

    private class OtaFileTransfer implements FileTransactionModelCallback {

        @Override
        public void onCancelForReceiveFile() {
        }

        @Override
        public void onChannelAvailable(boolean arg0) {
            IwdsLog.d(TAG, "OtaFiletransfer data available :" + arg0);
            if (!arg0) {
                mHandler.obtainMessage(MSG_CHANNEL_UNAVAILABLE).sendToTarget();
            } else {
                mHandler.obtainMessage(MSG_CHANNEL_AVAILABLE).sendToTarget();
            }
        }

        @Override
        public void onConfirmForReceiveFile() {
            IwdsLog.d(TAG, "OtaFiletransfer onConfirmForReceiveFile");
        }

        @Override
        public void onFileArrived(File arg0) {
            String md5 = "";
            // 生成MD5并保存
            try {
                md5 = Utils.getMd5ByFile(arg0);
                Utils.setUpdatezipMD5(getBaseContext(), md5);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            long len = arg0.length();
            IwdsLog.d(TAG, "OtaFiletransfer onFileArrived :" + arg0.getAbsolutePath() + "file md5:" + md5);
            String cmd = "md5#" + md5 + "#" + "true" + "#" + len;
            IwdsLog.d(TAG, "+++++send updatezip md5 to phone:" + cmd);
            IwdsLog.d(TAG, "file length is error, FileInfo.length:" + mLength + "  arrived length: " + len);
            mFiletransModel.sendCmd(cmd);
            mHandler.obtainMessage(MSG_FILEARRIVD).sendToTarget();
        }

        @Override
        public void onLinkConnected(DeviceDescriptor arg0, boolean arg1) {
            IwdsLog.d(TAG, "OtaFiletransfer onLinkConnected " + arg0.toString() + " isConnected: "  + arg1);
            if (!arg1) {
                mHandler.obtainMessage(MSG_DISCONNECT).sendToTarget();
            }
        }

        @Override
        public void onRecvFileProgress(int arg0) {
            mHandler.obtainMessage(MSG_SET_PROGRESS, arg0, 0).sendToTarget();
        }

        @Override
        public void onRequestSendFile(FileInfo arg0) {
            mLength = arg0.length;
            IwdsLog.d(TAG, "onRequestSendFile: file name: " + arg0.name + " length: " + mLength);
            mHandler.obtainMessage(MSG_REQUESTSENDFILE).sendToTarget();
        }

        @Override
        public void onSendFileProgress(int arg0) {

        }

        @Override
        public void onSendResult(DataTransactResult arg0) {

        }

        @Override
        public void onFileTransferError(int arg0) {

        }

        @Override
        public void onRecvFileInterrupted(int arg0) {

        }

        @Override
        public void onSendFileInterrupted(int arg0) {

        }
    }

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REPONE_CMD:
                    mFiletransModel.sendCmd(msg.obj.toString());
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    // MasterClearReceiver.java to receive . and reboot
                    IwdsLog.d(TAG, "+++++recovery:" + mOtapath);
                    Intent in = new Intent("android.intent.action.MASTER_CLEAR");
                    in.putExtra("ota", "ota");
                    // in.putExtra("command", "--remove_package");
                    in.putExtra("filename", mOtapath);
                    getApplicationContext().sendBroadcast(in);
                    break;

                case MSG_REQUESTSENDFILE:
                    /* 接收文件之前，先把之前保存的md5码删除 */
                    Utils.setUpdatezipMD5(getBaseContext(), Utils.DEFAULT_UPDATE_ZIP_MD5);
                    mFiletransModel.notifyConfirmForReceiveFile();
                    showProgressDialog();
                    break;
                case MSG_CHANNEL_AVAILABLE:
                    String md5 = Utils.getUpdatezipMD5(getBaseContext());
                    boolean bExist = Utils.updatezipIsExist();
                    String cmd = "md5#" + md5 + "#" + bExist;
                    IwdsLog.i(TAG, "+++++send updatezip md5 to phone:" + cmd);
                    mFiletransModel.sendCmd(cmd);
                    String url ="otaurl#" +  getApplicationContext().getString(R.string.otaurl);
                    IwdsLog.i(TAG, "+++++send otaurl to phone:" + url);
                    mFiletransModel.sendCmd(url);
                    break;
                case MSG_CHANNEL_UNAVAILABLE:
                case MSG_DISCONNECT:
                case MSG_FILEARRIVD:
                    dimissProgressDialog();
                    break;

                case MSG_SET_PROGRESS:
                    mProgress.setProgress(msg.arg1);
                    break;
            }
            super.handleMessage(msg);
        }
    };
}
