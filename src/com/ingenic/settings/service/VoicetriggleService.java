/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * WuZhiMing(Mike)<zhiming.wu@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 675 Mass
 * Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.SystemClock;
import android.os.PowerManager.WakeLock;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;

import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.smartsense.Sensor;
import com.ingenic.iwds.smartsense.SensorEvent;
import com.ingenic.iwds.smartsense.SensorEventListener;
import com.ingenic.iwds.smartsense.SensorServiceManager;

public class VoicetriggleService extends Service implements
        ServiceClient.ConnectionCallbacks {
    private final static String TAG = "Voicetriggle";
    private final static String Voicetriggle_STATE = "Voicetriggle_on";

    private Context mcontext;

    private Sensor mVoiceTriggle;
    private ServiceClient mClient;
    private SensorServiceManager mService;
    private WakeLock mWakeLock;
    private PowerManager mPowerManager;

    private static int voicetriggle_state = 0;
    private static boolean enabled = false;

    private SharedPreferences mVoicetrigglePreferences;
    private SharedPreferences.OnSharedPreferenceChangeListener mVoicetrigglePreferencesListener;

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        mcontext = getBaseContext();
        if (mPowerManager == null)
            mPowerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (mWakeLock == null)
            mWakeLock = mPowerManager.newWakeLock(
                    PowerManager.SCREEN_DIM_WAKE_LOCK
                            | PowerManager.ACQUIRE_CAUSES_WAKEUP,
                    "Setting-VoiceTriggle");

        if (mcontext == null || mWakeLock == null) {
            Log.e(TAG, "context || wakelock is null");
        } else {
            mClient = new ServiceClient(mcontext,
                    ServiceManagerContext.SERVICE_SENSOR, this);
            mClient.connect();
        }
        mVoicetrigglePreferences = mcontext.getSharedPreferences(TAG,
                Context.MODE_PRIVATE);
        mVoicetrigglePreferencesListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(
                    SharedPreferences sharedPreferences, String key) {
                voicetriggle_state = getIntPref(mcontext, Voicetriggle_STATE, 0);
                if (voicetriggle_state > 0) {
                    enabled_Voicetriggle(true);
                } else {
                    enabled_Voicetriggle(false);
                }
            }
        };
        mVoicetrigglePreferences
                .registerOnSharedPreferenceChangeListener(mVoicetrigglePreferencesListener);

        voicetriggle_state = getIntPref(mcontext, Voicetriggle_STATE, 0);
        if (voicetriggle_state > 0) {
            enabled_Voicetriggle(true);
        } else {
            enabled_Voicetriggle(false);
        }
    }

    @Override
    public void onDestroy() {
        mVoicetrigglePreferences
                .unregisterOnSharedPreferenceChangeListener(mVoicetrigglePreferencesListener);
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // TODO Auto-generated method stub
        return super.onUnbind(intent);
    }

    private static int getIntPref(Context context, String name, int def) {
        SharedPreferences prefs = context.getSharedPreferences(TAG,
                Context.MODE_PRIVATE);
        int ret = prefs.getInt(name, def);
        return ret;
    }

    private static void setIntPref(Context context, String name, int value) {
        SharedPreferences prefs = context.getSharedPreferences(TAG,
                Context.MODE_PRIVATE);
        Editor ed = prefs.edit();
        ed.putInt(name, value);
        ed.commit();
    }

    @Override
    public void onConnected(ServiceClient serviceClient) {
        Log.i(TAG, "connnect to service!");
        mService = (SensorServiceManager) mClient.getServiceManagerContext();
        mVoiceTriggle = mService.getDefaultSensor(Sensor.TYPE_VOICE_TRIGGER);
        voicetriggle_state = getIntPref(mcontext, Voicetriggle_STATE, 0);
        if (voicetriggle_state > 0) {
            enabled_Voicetriggle(true);
        } else {
            enabled_Voicetriggle(false);
        }

    }

    @Override
    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {
        Log.e(TAG, "Sensor service Disconnnected. ");
    }

    @Override
    public void onConnectFailed(ServiceClient serviceClient,
            ConnectFailedReason reason) {
        Log.e(TAG, "Sensor service connect fail.");
    }

    private void enabled_Voicetriggle(boolean enable) {
        if (mVoiceTriggle == null) {
            Log.e(TAG, "mGesture is uninit, return!");
            return;
        }
        if (mListener == null) {
            Log.e(TAG, "mListener is unnit, return!");
        }
        if (enable && (!enabled)) {
            // if has enabled, then leave.
            enabled = true;
            Log.i(TAG, "Voice triggle enabled!");
            mService.registerListener(mListener, mVoiceTriggle, 0);
            setIntPref(mcontext, Voicetriggle_STATE, 1);
        } else if ((!enable) && (enabled)) {
            // if has disabled, then leave.
            enabled = false;
            Log.i(TAG, "Voice triggle disabled!");
            mService.unregisterListener(mListener, mVoiceTriggle);
            setIntPref(mcontext, Voicetriggle_STATE, 0);
        }
    }

    private SensorEventListener mListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.sensorType == Sensor.TYPE_VOICE_TRIGGER) {
                if (!(mWakeLock.isHeld())) {
                    mWakeLock.acquire(3 * 1000);
                }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };
}
