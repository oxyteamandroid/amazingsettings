package com.ingenic.settings.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.settings.Utils;

public class DeviceModelReceiver extends BroadcastReceiver {
    static final String ACTION = "android.intent.action.BOOT_COMPLETED";

    @Override
    public void onReceive(Context arg0, Intent arg1) {
        // TODO Auto-generated method stub
        if (arg1.getAction().equals(ACTION) || arg1.getAction().equals(Utils.ACTION_RESTART_SERVICE)) {
            arg0.startService(new Intent(arg0, DeviceModelService.class));
            IwdsLog.d(this, "DeviceModelService start!");
            arg0.startService(new Intent(arg0, OtaService.class));
            IwdsLog.d(this, "OtaService start!");
            arg0.startService(new Intent(arg0, GestureService.class));
            IwdsLog.d(this, "Gesture start!");
            arg0.startService(new Intent(arg0, VoicetriggleService.class));
            IwdsLog.d(this, "Voicetriggle start!");
        }
    }

}
