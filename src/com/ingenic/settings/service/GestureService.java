/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * WuZhiMing(Mike)<zhiming.wu@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 675 Mass
 * Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings.service;

import java.io.File;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.SystemClock;
import android.os.PowerManager.WakeLock;
import android.util.Log;

import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.devicemanager.DeviceManagerServiceManager;
import com.ingenic.iwds.smartsense.Sensor;
import com.ingenic.iwds.smartsense.SensorEvent;
import com.ingenic.iwds.smartsense.SensorEventListener;
import com.ingenic.iwds.smartsense.SensorServiceManager;

public class GestureService extends Service implements
        ServiceClient.ConnectionCallbacks {
    private final static String TAG = "Gesture";
    private final static String GESTURE_STATE = "gesture_on";
    private final static int TRY_CONNECT_TIMES = 20;
    private final static long DEFAULT_HAND_DOWN_TIME = 600; // ms
    private final static int LEFT_HAND_WEAR = 0;
    private final static int RIGHT_HAND_WEAR = 1;
    private final static String SET_WITCH_HAND_WEAR_PATH = "/sys/frizz/set_right_hand_wear";

    private static int had_try_times = 0;
    private Context mcontext;
    // Gesture sensor
    private Sensor mGesture;
    private ServiceClient mClient;
    private SensorServiceManager mService;
    private WakeLock mWakeLock;
    private PowerManager mPowerManager;
    // Device Manager for right-hand-setting
    private ServiceClient m_DeviceManager_client;
    private DeviceManagerServiceManager m_DeviceManager_service;
    private DeviceManager_CallBack m_DeviceManager_cb = new DeviceManager_CallBack();

    private static int gesture_state = 0;
    private static long time_now = 0;
    private static boolean enabled = false;
    private static int hand_setted = -1;

    private SharedPreferences mGesturePreferences;
    private SharedPreferences.OnSharedPreferenceChangeListener mGesturePreferencesListener;

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        mcontext = getBaseContext();
        if (mPowerManager == null)
            mPowerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (mWakeLock == null)
            mWakeLock = mPowerManager.newWakeLock(
                    PowerManager.SCREEN_DIM_WAKE_LOCK
                            | PowerManager.ACQUIRE_CAUSES_WAKEUP,
                    "Setting-Gesture");

        if (mcontext == null || mWakeLock == null) {
            Log.e(TAG, "context || wakelock is null");
        } else {
            mWakeLock.setReferenceCounted(false);
            mClient = new ServiceClient(mcontext,
                    ServiceManagerContext.SERVICE_SENSOR, this);
            m_DeviceManager_client = new ServiceClient(this,
                    ServiceManagerContext.SERVICE_DEVICE_MANAGER,
                    m_DeviceManager_cb);
            m_DeviceManager_client.connect();
            // mClient.connect();
            mGesturePreferences = mcontext.getSharedPreferences(TAG,
                    Context.MODE_PRIVATE);
            mGesturePreferencesListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
                @Override
                public void onSharedPreferenceChanged(
                        SharedPreferences sharedPreferences, String key) {
                    gesture_state = getIntPref(mcontext, GESTURE_STATE, 0);
                    if (gesture_state > 0) {
                        enabled_gesture(true);
                        set_witch_hand_wear(gesture_state);
                    } else {
                        enabled_gesture(false);
                    }
                }
            };
            mGesturePreferences
                    .registerOnSharedPreferenceChangeListener(mGesturePreferencesListener);
        }
    }

    @Override
    public void onDestroy() {
        mGesturePreferences
                .unregisterOnSharedPreferenceChangeListener(mGesturePreferencesListener);
        mService.unregisterListener(mListener, mGesture);
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // TODO Auto-generated method stub
        return super.onUnbind(intent);
    }

    private SensorEventListener mListener = new SensorEventListener() {
        // time_now is a trick, for human raise hand or let hand down needs more
        // than a time limit,
        // so if faster than that, ignore it.
        // in otherwise, if the gesture's accuracy is very high, the
        // DEFAULT_HAND_DOWN_TIME should be set as 0.
        @Override
        public void onSensorChanged(SensorEvent event) {
            int gesture_value = 0;
            if (event.sensorType == Sensor.TYPE_GESTURE) {
                gesture_value = (int) event.values[0];
                if (gesture_value == SensorEvent.GESTURE_RAISE_HAND_AND_LOOK) {
                    if (!(mWakeLock.isHeld())) {
                        Log.i(TAG,
                                "Gesture wake at time : "
                                        + SystemClock.uptimeMillis());
                        time_now = SystemClock.uptimeMillis();
                        mWakeLock.acquire(3 * 1000);
                    }
                } else if (gesture_value == SensorEvent.GESTURE_LET_HAND_DOWN_AFTER_LOOK) {
                    if (time_now + DEFAULT_HAND_DOWN_TIME < SystemClock
                            .uptimeMillis()) {
                        if (mWakeLock.isHeld()) {
                            mWakeLock.release();
                        }
                        // shutdown the screen now
                        mPowerManager.goToSleep(SystemClock.uptimeMillis());
                        Log.i(TAG,
                                "Gesture wake release : "
                                        + SystemClock.uptimeMillis());
                    }
                }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    private void enabled_gesture(boolean enable) {
        if (mGesture == null) {
            Log.e(TAG, "mGesture is uninit, return!");
            return;
        }
        if (mListener == null) {
            Log.e(TAG, "mListener is uninit, return!");
        }
        if (mService == null) {
            Log.e(TAG, "mService is uninit, return!");
        }
        if (enable && (!enabled)) {
            // if has enabled, then leave.
            enabled = true;
            Log.i(TAG, "Gesture enabled!");
            mService.registerListener(mListener, mGesture, 0);
            //setIntPref(mcontext, GESTURE_STATE, 1);
        } else if ((!enable) && (enabled)) {
            // if has disabled, then leave.
            enabled = false;
            Log.i(TAG, "Gesture disabled!");
            mService.unregisterListener(mListener, mGesture);
            setIntPref(mcontext, GESTURE_STATE, 0);
        }
    }

    class DeviceManager_CallBack implements ServiceClient.ConnectionCallbacks {
        @Override
        public void onConnected(ServiceClient serviceClient) {
            m_DeviceManager_service = (DeviceManagerServiceManager) m_DeviceManager_client
                    .getServiceManagerContext();
            mClient.connect();
        }

        @Override
        public void onDisconnected(ServiceClient serviceClient,
                boolean unexpected) {

        }

        @Override
        public void onConnectFailed(ServiceClient serviceClient,
                ConnectFailedReason reason) {

        }
    };

    private void set_witch_hand_wear(int hand) {
        hand = hand - 1;
        if (hand > 1 || hand < 0) {
            Log.e(TAG, "set_left_hand_wear, out of range, check it!! hand = "
                    + hand);
            return;
        }
        if (hand != hand_setted) {
            hand_setted = hand;
            // remap the code for driver.
            hand = hand > 0 ? 0 : 1;
            if (hand == RIGHT_HAND_WEAR) {
                m_DeviceManager_service.setWearOnRightHand(true);
            } else if (hand == LEFT_HAND_WEAR) {
                m_DeviceManager_service.setWearOnRightHand(false);
            }
        }
    }

    private static int getIntPref(Context context, String name, int def) {
        SharedPreferences prefs = context.getSharedPreferences(TAG,
                Context.MODE_PRIVATE);
        int ret = prefs.getInt(name, def);
        return ret;
    }

    private static void setIntPref(Context context, String name, int value) {
        SharedPreferences prefs = context.getSharedPreferences(TAG,
                Context.MODE_PRIVATE);
        Editor ed = prefs.edit();
        ed.putInt(name, value);
        ed.commit();
    }

    @Override
    public void onConnected(ServiceClient serviceClient) {
        Log.i(TAG, "connnect to service!");
        mService = (SensorServiceManager) mClient.getServiceManagerContext();
        mGesture = mService.getDefaultSensor(Sensor.TYPE_GESTURE);
        gesture_state = getIntPref(mcontext, GESTURE_STATE, 0);
        if (gesture_state > 0) {
            enabled_gesture(true);
            set_witch_hand_wear(gesture_state);
        } else {
            enabled_gesture(false);
        }

    }

    @Override
    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {
        Log.e(TAG, "Sensor service Disconnnected, try connect. times : "
                + had_try_times);
        if (had_try_times < TRY_CONNECT_TIMES) {
            mClient.connect();
            had_try_times += 1;
        }

    }

    @Override
    public void onConnectFailed(ServiceClient serviceClient,
            ConnectFailedReason reason) {
        Log.e(TAG, "Sensor service connect fail, try connect. times : "
                + had_try_times);
        if (had_try_times < TRY_CONNECT_TIMES) {
            mClient.connect();
            had_try_times += 1;
        }
    }

}
