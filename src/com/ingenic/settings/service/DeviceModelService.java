package com.ingenic.settings.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.app.ActivityManagerNative;
import android.app.AlarmManager;
import android.app.IActivityManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.app.backup.BackupManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemClock;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.LocaleTransactionModel;
import com.ingenic.iwds.datatransactor.elf.SyncTimeInfo;
import com.ingenic.iwds.datatransactor.elf.SyncTimeTransactionModel;
import com.ingenic.iwds.datatransactor.elf.SyncTimeTransactionModel.SyncTimeTransactionModelCallback;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.settings.Utils;

public class DeviceModelService extends Service {
    private static final String UUID_LOCALE = "735f9b94-c67b-5a2a-20d1-79c29ed7a300";
    private static final String UUID_SYNCTIME = "bb1609ec-d29d-11e4-ab8f-5404a6abe086";
    private static LocaleTransactionModel mLocaleModel;
    private static SyncTimeTransactionModel mSyncTimeModel;
    private Context mContext;

    private LocaleTransactionModel.LocaleTransactionModelCallback mLocaleCallback = new LocaleTransactionModel.LocaleTransactionModelCallback() {

        @Override
        public void onChannelAvailable(boolean arg0) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onLinkConnected(DeviceDescriptor arg0, boolean arg1) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onObjectArrived(Locale arg0) {
            // TODO Auto-generated method stub
            Locale loc = arg0;
            updateLocale(loc);
        }

        @Override
        public void onRequest() {
            // TODO Auto-generated method stub
        }

        @Override
        public void onRequestFailed() {
            // TODO Auto-generated method stub
        }

        @Override
        public void onSendResult(DataTransactResult arg0) {
            // TODO Auto-generated method stub
        }
    };
    private SyncTimeTransactionModelCallback mSyncTimeCallback = new SyncTimeTransactionModelCallback() {

        @Override
        public void onSendResult(DataTransactResult result) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onRequestFailed() {
            // TODO Auto-generated method stub

        }

        @Override
        public void onRequest() {
            // TODO Auto-generated method stub

        }

        @Override
        public void onObjectArrived(SyncTimeInfo object) {
            // TODO Auto-generated method stub
            IwdsLog.d(this, "onObjectArrived synctime : " + object.toString());
            if (object.currenttime / 1000 < Integer.MAX_VALUE) {
                ((AlarmManager) mContext
                        .getSystemService(Context.ALARM_SERVICE))
                        .setTime(object.currenttime);
            }
            ((AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE))
                    .setTimeZone(object.timezoneid);
        }

        @Override
        public void onLinkConnected(DeviceDescriptor descriptor,
                boolean isConnected) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onChannelAvailable(boolean isAvailable) {
            // TODO Auto-generated method stub

        }
    };

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        // 使得服务不得被杀死
        CharSequence label = getApplication().getApplicationInfo().loadLabel(
                getPackageManager());

        Notification notification = new Notification(getApplication()
                .getApplicationInfo().icon, label, System.currentTimeMillis());

        Intent it = new Intent(
                ServiceManagerContext.ACTION_NOTIFICATION_CLICKED);
        it.setFlags(it.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, it, 0);
        notification.setLatestEventInfo(this, label, "", pendingIntent);
        notification.flags = notification.flags
                | Notification.FLAG_ONGOING_EVENT;

        startForeground(9990, notification);
        mContext = getBaseContext();
        mLocaleModel = new LocaleTransactionModel(mContext, mLocaleCallback,
                UUID_LOCALE);
        mLocaleModel.start();
        mSyncTimeModel = new SyncTimeTransactionModel(mContext,
                mSyncTimeCallback, UUID_SYNCTIME);
        mSyncTimeModel.start();

        IwdsLog.d(this, "DeviceModelService onCreate!");
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        mLocaleModel.stop();
        mSyncTimeModel.stop();
        // 在异常退出时保证服务畅通
        sendBroadcast(new Intent(Utils.ACTION_RESTART_SERVICE));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // TODO Auto-generated method stub
        return super.onUnbind(intent);
    }

    private void setDateTime(int year, int month, int day, int hour, int minute)
            throws IOException, InterruptedException {

        Calendar calender = Calendar.getInstance();
        calender.set(Calendar.YEAR, year);
        calender.set(Calendar.MONTH, month - 1);
        calender.set(Calendar.DAY_OF_MONTH, day);
        calender.set(Calendar.HOUR_OF_DAY, hour);
        calender.set(Calendar.MINUTE, minute);

        long when = calender.getTimeInMillis();
        if (when / 1000 < Integer.MAX_VALUE) {
            SystemClock.setCurrentTimeMillis(when);
        }

        long now = Calendar.getInstance().getTimeInMillis();

        SimpleDateFormat formatter = new SimpleDateFormat(
                "yyyy年MM月dd日    HH:mm:ss     ");
        Date curDate = new Date(System.currentTimeMillis());
        String str = formatter.format(curDate);
        IwdsLog.d(this, "setDateTime is: " + str);

        if (now - when > 2000)
            throw new IOException("failed to set Date.");

    }

    private void setDate(int year, int month, int day) throws IOException,
            InterruptedException {

        Calendar calender = Calendar.getInstance();

        calender.set(Calendar.YEAR, year);
        calender.set(Calendar.MONTH, month - 1);
        calender.set(Calendar.DAY_OF_MONTH, day);

        long when = calender.getTimeInMillis();

        if (when / 1000 < Integer.MAX_VALUE) {
            SystemClock.setCurrentTimeMillis(when);
        }

        long now = Calendar.getInstance().getTimeInMillis();
        // Log.d(TAG, "set tm="+when + ", now tm="+now);

        if (now - when > 2000)
            throw new IOException("failed to set Date.");
    }

    private void setTime(int hour, int minute) throws IOException,
            InterruptedException {

        Calendar calender = Calendar.getInstance();

        calender.set(Calendar.HOUR_OF_DAY, hour);
        calender.set(Calendar.MINUTE, minute);
        long when = calender.getTimeInMillis();

        if (when / 1000 < Integer.MAX_VALUE) {
            SystemClock.setCurrentTimeMillis(when);
        }

        long now = Calendar.getInstance().getTimeInMillis();
        // Log.d(TAG, "set tm="+when + ", now tm="+now);

        if (now - when > 2000)
            throw new IOException("failed to set Time.");
    }

    private void updateLocale(Locale locale) {
        try {
            Locale[] locales = Locale.getAvailableLocales();
            // print locales
            IwdsLog.d(this, "updateLocale Language: " + locale.getLanguage()
                    + " Country : " + locale.getCountry());
            for (int i = 0; i < locales.length; i++) {
                if (locales[i].getLanguage().equals(locale.getLanguage()) &&
                        locales[i].getCountry().equals(locale.getCountry())) {
                    IActivityManager am = ActivityManagerNative.getDefault();
                    Configuration config = am.getConfiguration();
                    config.locale = locale;
                    // indicate this isn't some passing default - the user wants
                    // this
                    // remembered
                    config.userSetLocale = true;
                    am.updateConfiguration(config);

                    // Trigger the dirty bit for the Settings Provider.
                    BackupManager.dataChanged("com.android.providers.settings");
                    IwdsLog.d(this, "updateLocale ok ");

                }
            }
        } catch (RemoteException e) {
            // Intentionally left blank
        }
    }
}
