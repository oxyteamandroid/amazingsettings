/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Vibrator;
import android.provider.Settings;

public class VibrateEnabler extends Enabler {
    private Context mContext;

    public VibrateEnabler(Context context) {
        mContext = context;
    }

    @Override
    public void setEnable(boolean enabled) {
        Settings.System.putInt(mContext.getContentResolver(), Settings.System.VIBRATE_ON,
                enabled ? 1 : 0);
        if (enabled) vibrator();
        setChecked(isEnabled());
        performStatusChange(enabled);
    }

    private void vibrator() {
        Vibrator vibrator = (Vibrator) mContext.getSystemService(Service.VIBRATOR_SERVICE);
        vibrator.vibrate(100);
    }

    @Override
    public boolean isEnabled() {
        ContentResolver resolver = mContext.getContentResolver();
        return Settings.System.getInt(resolver, Settings.System.VIBRATE_ON, 0) != 0;
    }
}
