/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings.bluetooth;

import android.app.NotificationManager;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

/**
 * BluetoothPairingRequest is a receiver for any Bluetooth pairing request. It checks if the
 * Bluetooth Settings is currently visible and brings up the PIN, the passkey or a confirmation
 * entry dialog. Otherwise it puts a Notification in the status bar, which can be clicked to bring
 * up the Pairing entry dialog.
 */
public final class BluetoothPairingRequest extends BroadcastReceiver {

    private static final int NOTIFICATION_ID = android.R.drawable.stat_sys_data_bluetooth;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(BluetoothDevice.ACTION_PAIRING_REQUEST)) {
            // convert broadcast intent into activity intent (same action string)
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            int type = intent.getIntExtra(BluetoothDevice.EXTRA_PAIRING_VARIANT,
                    BluetoothDevice.ERROR);
            int pairingKey = intent.getIntExtra(BluetoothDevice.EXTRA_PAIRING_KEY,
                    BluetoothDevice.ERROR);
            pair(device, type, pairingKey);

            PowerManager powerManager = (PowerManager) context
                    .getSystemService(Context.POWER_SERVICE);

            PowerManager.WakeLock mWakeLock = powerManager
                    .newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.FULL_WAKE_LOCK,
                            "RequestPair");

            if (!powerManager.isScreenOn()) {
                if (!mWakeLock.isHeld()) mWakeLock.acquire(5000);
            }

        } else if (action.equals(BluetoothDevice.ACTION_PAIRING_CANCEL)) {
            // Remove the notification
            NotificationManager manager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            manager.cancel(NOTIFICATION_ID);
        }
    }

    private void pair(BluetoothDevice device, int type, int passKey) {
        if (device == null || type == BluetoothDevice.ERROR) {
            return;
        }
        switch (type) {
        case BluetoothDevice.PAIRING_VARIANT_PIN:
            device.setPin("1234".getBytes());
            break;
        case BluetoothDevice.PAIRING_VARIANT_PASSKEY:
            device.setPasskey(1234);
            break;
        case BluetoothDevice.PAIRING_VARIANT_PASSKEY_CONFIRMATION:
        case BluetoothDevice.PAIRING_VARIANT_CONSENT:
        case BluetoothDevice.PAIRING_VARIANT_DISPLAY_PASSKEY:
            device.setPairingConfirmation(true);
            break;
        case BluetoothDevice.PAIRING_VARIANT_DISPLAY_PIN:
            if (passKey == BluetoothDevice.ERROR) {
                return;
            }
            String pairingKey = String.format("%04d", passKey);
            device.setPin(pairingKey.getBytes());
            break;
        case BluetoothDevice.PAIRING_VARIANT_OOB_CONSENT:
            device.setRemoteOutOfBandData();
            break;
        default:
            break;
        }
    }
}
