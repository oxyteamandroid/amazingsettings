/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Apps/AmazingSettings Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.settings.bluetooth;

/**
 * BluetoothCallback provides a callback interface for the settings UI to receive events from
 * {@link BluetoothEventManager}.
 */
interface BluetoothCallback {
    void onBluetoothStateChanged(int bluetoothState);

    void onScanningStateChanged(boolean started);

    void onDeviceAdded(CachedBluetoothDevice cachedDevice);

    void onDeviceDeleted(CachedBluetoothDevice cachedDevice);

    void onDeviceBondStateChanged(CachedBluetoothDevice cachedDevice, int bondState);
}
